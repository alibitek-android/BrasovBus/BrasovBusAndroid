package ro.krogan.brasovbus.preferences;

import ro.krogan.brasovbus.R;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
public class PreferenceFragmentActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preferences_layout);
		android.app.FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		PreferencesFragment fragment1 = new PreferencesFragment();
		fragmentTransaction.replace(android.R.id.content, fragment1);
		fragmentTransaction.commit();
	}
}
