package ro.krogan.brasovbus.preferences;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import ro.krogan.brasovbus.R;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
public class PreferencesFragment extends PreferenceFragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
	
}
