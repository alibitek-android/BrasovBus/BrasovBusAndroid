package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties({ "_id" })
public class Agency implements Serializable {

	private static final long serialVersionUID = 5559435854261032848L;

	private String agency_id;

	private String agency_name;

	private String agency_url;

	private String agency_timezone;

	private String agency_lang;

	public Agency() {
	}

	public Agency(String agency_id, String agency_name, String agency_url,
			String agency_timezone, String agency_lang) {
		this.agency_id = agency_id;
		this.agency_name = agency_name;
		this.agency_url = agency_url;
		this.agency_timezone = agency_timezone;
		this.agency_lang = agency_lang;
	}

	public String getAgency_id() {
		return agency_id;
	}

	public void setAgency_id(final String agency_id) {
		this.agency_id = agency_id;
	}

	public String getAgency_name() {
		return agency_name;
	}

	public void setAgency_name(final String agency_name) {
		this.agency_name = agency_name;
	}

	public String getAgency_url() {
		return agency_url;
	}

	public void setAgency_url(final String agency_url) {
		this.agency_url = agency_url;
	}

	public String getAgency_timezone() {
		return agency_timezone;
	}

	public void setAgency_timezone(final String agency_timezone) {
		this.agency_timezone = agency_timezone;
	}

	public String getAgency_lang() {
		return agency_lang;
	}

	public void setAgency_lang(final String agency_lang) {
		this.agency_lang = agency_lang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((agency_id == null) ? 0 : agency_id.hashCode());
		result = (prime * result)
				+ ((agency_lang == null) ? 0 : agency_lang.hashCode());
		result = (prime * result)
				+ ((agency_name == null) ? 0 : agency_name.hashCode());
		result = (prime * result)
				+ ((agency_timezone == null) ? 0 : agency_timezone.hashCode());
		result = (prime * result)
				+ ((agency_url == null) ? 0 : agency_url.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Agency other = (Agency) obj;
		if (agency_id == null) {
			if (other.agency_id != null) {
				return false;
			}
		} else if (!agency_id.equals(other.agency_id)) {
			return false;
		}
		if (agency_lang == null) {
			if (other.agency_lang != null) {
				return false;
			}
		} else if (!agency_lang.equals(other.agency_lang)) {
			return false;
		}
		if (agency_name == null) {
			if (other.agency_name != null) {
				return false;
			}
		} else if (!agency_name.equals(other.agency_name)) {
			return false;
		}
		if (agency_timezone == null) {
			if (other.agency_timezone != null) {
				return false;
			}
		} else if (!agency_timezone.equals(other.agency_timezone)) {
			return false;
		}
		if (agency_url == null) {
			if (other.agency_url != null) {
				return false;
			}
		} else if (!agency_url.equals(other.agency_url)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Agency [agency_id=" + agency_id + ", agency_name="
				+ agency_name + ", agency_url=" + agency_url
				+ ", agency_timezone=" + agency_timezone + ", agency_lang="
				+ agency_lang + "]";
	}
}
