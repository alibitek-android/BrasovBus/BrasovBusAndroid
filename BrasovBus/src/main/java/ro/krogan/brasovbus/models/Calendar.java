package ro.krogan.brasovbus.models;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
public class Calendar implements Serializable {
	private static final long serialVersionUID = 8267290640522345004L;

	public String service_id;
	public String monday;
	public String tuesday;
	public String wednesday;
	public String thursday;
	public String friday;
	public String saturday;
	public String sunday;
	public String start_date;
	public String end_date;
}
