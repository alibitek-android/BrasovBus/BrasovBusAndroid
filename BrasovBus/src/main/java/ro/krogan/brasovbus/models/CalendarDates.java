package ro.krogan.brasovbus.models;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
public class CalendarDates implements Serializable {

	private static final long serialVersionUID = 2840143717739614236L;
	public String service_id;
	public String date;
	public String exception_type;
}
