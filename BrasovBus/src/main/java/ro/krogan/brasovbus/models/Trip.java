package ro.krogan.brasovbus.models;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
public class Trip implements Serializable {
	private static final long serialVersionUID = -2354876511158647191L;
	public String route_id;
	public String service_id;
	public String trip_id;
	public String trip_headsign;
	public String direction_id;
	public String block_id;
}
