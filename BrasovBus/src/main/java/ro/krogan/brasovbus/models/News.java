package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties({ "_id" })
public class News implements Serializable, Comparable<News> {
	private static final long serialVersionUID = -1877644606486216329L;
	private Integer id;
	private Long date;
	private String contents;
	private String title;
	private String subtitle;

	public News() {
	}

	public News(Integer id, Long date, String contents, String title,
			String subtitle) {
		this.id = id;
		this.date = date;
		this.contents = contents;
		this.title = title;
		this.subtitle = subtitle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		News news = (News) o;

		if (contents != null ? !contents.equals(news.contents)
				: news.contents != null) {
			return false;
		}
		if (date != null ? !date.equals(news.date) : news.date != null) {
			return false;
		}
		if (id != null ? !id.equals(news.id) : news.id != null) {
			return false;
		}
		if (subtitle != null ? !subtitle.equals(news.subtitle)
				: news.subtitle != null) {
			return false;
		}
		if (title != null ? !title.equals(news.title) : news.title != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (contents != null ? contents.hashCode() : 0);
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (subtitle != null ? subtitle.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "News{" + "id=" + id + ", date=" + date + ", contents='"
				+ contents + '\'' + ", title='" + title + '\'' + ", subtitle='"
				+ subtitle + '\'' + '}';
	}

	@Override
	public int compareTo(News news) {
		// Date d1 = new Date(Long.valueOf(String.valueOf(date).replace("000",
		// "")));
		// Date d2 = new
		// Date(Long.valueOf(String.valueOf(news.getDate()).replace("000",
		// "")));
		Date d1 = new Date(date);
		Date d2 = new Date(news.getDate());

		return -d1.compareTo(d2);
	}
}
