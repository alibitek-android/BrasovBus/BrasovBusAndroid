package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties({ "_id" })
public class Route implements Serializable {

	private static final long serialVersionUID = -230404432524011948L;

	public String route_id;

	public String agency_id;

	public String route_short_name;

	public String route_long_name;

	public String route_desc;

	public String route_type;

	public String route_url;

	public String route_color;

	public String route_text_color;

	public Route() {
	}

	public Route(String route_id, String agency_id, String route_short_name,
			String route_long_name, String route_desc, String route_type,
			String route_url, String route_color, String route_text_color) {
		this.route_id = route_id;
		this.agency_id = agency_id;
		this.route_short_name = route_short_name;
		this.route_long_name = route_long_name;
		this.route_desc = route_desc;
		this.route_type = route_type;
		this.route_url = route_url;
		this.route_color = route_color;
		this.route_text_color = route_text_color;
	}

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

	public String getAgency_id() {
		return agency_id;
	}

	public void setAgency_id(String agency_id) {
		this.agency_id = agency_id;
	}

	public String getRoute_short_name() {
		return route_short_name;
	}

	public void setRoute_short_name(String route_short_name) {
		this.route_short_name = route_short_name;
	}

	public String getRoute_long_name() {
		return route_long_name;
	}

	public void setRoute_long_name(String route_long_name) {
		this.route_long_name = route_long_name;
	}

	public String getRoute_desc() {
		return route_desc;
	}

	public void setRoute_desc(String route_desc) {
		this.route_desc = route_desc;
	}

	public String getRoute_type() {
		return route_type;
	}

	public void setRoute_type(String route_type) {
		this.route_type = route_type;
	}

	public String getRoute_url() {
		return route_url;
	}

	public void setRoute_url(String route_url) {
		this.route_url = route_url;
	}

	public String getRoute_color() {
		return route_color;
	}

	public void setRoute_color(String route_color) {
		this.route_color = route_color;
	}

	public String getRoute_text_color() {
		return route_text_color;
	}

	public void setRoute_text_color(String route_text_color) {
		this.route_text_color = route_text_color;
	}

	@Override
	public String toString() {
		return "Route{" + "route_id='" + route_id + '\'' + ", agency_id='"
				+ agency_id + '\'' + ", route_short_name='" + route_short_name
				+ '\'' + ", route_long_name='" + route_long_name + '\''
				+ ", route_desc='" + route_desc + '\'' + ", route_type='"
				+ route_type + '\'' + ", route_url='" + route_url + '\''
				+ ", route_color='" + route_color + '\''
				+ ", route_text_color='" + route_text_color + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Route route = (Route) o;

		if (agency_id != null ? !agency_id.equals(route.agency_id)
				: route.agency_id != null) {
			return false;
		}
		if (route_color != null ? !route_color.equals(route.route_color)
				: route.route_color != null) {
			return false;
		}
		if (route_desc != null ? !route_desc.equals(route.route_desc)
				: route.route_desc != null) {
			return false;
		}
		if (route_id != null ? !route_id.equals(route.route_id)
				: route.route_id != null) {
			return false;
		}
		if (route_long_name != null ? !route_long_name
				.equals(route.route_long_name) : route.route_long_name != null) {
			return false;
		}
		if (route_short_name != null ? !route_short_name
				.equals(route.route_short_name)
				: route.route_short_name != null) {
			return false;
		}
		if (route_text_color != null ? !route_text_color
				.equals(route.route_text_color)
				: route.route_text_color != null) {
			return false;
		}
		if (route_type != null ? !route_type.equals(route.route_type)
				: route.route_type != null) {
			return false;
		}
		if (route_url != null ? !route_url.equals(route.route_url)
				: route.route_url != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = route_id != null ? route_id.hashCode() : 0;
		result = 31 * result + (agency_id != null ? agency_id.hashCode() : 0);
		result = 31 * result
				+ (route_short_name != null ? route_short_name.hashCode() : 0);
		result = 31 * result
				+ (route_long_name != null ? route_long_name.hashCode() : 0);
		result = 31 * result + (route_desc != null ? route_desc.hashCode() : 0);
		result = 31 * result + (route_type != null ? route_type.hashCode() : 0);
		result = 31 * result + (route_url != null ? route_url.hashCode() : 0);
		result = 31 * result
				+ (route_color != null ? route_color.hashCode() : 0);
		result = 31 * result
				+ (route_text_color != null ? route_text_color.hashCode() : 0);
		return result;
	}
}
