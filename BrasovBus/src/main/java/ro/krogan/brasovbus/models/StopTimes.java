package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StopTimes implements Serializable {
	private static final long serialVersionUID = -4412344518545151881L;
	public String trip_id;
	public String arrival_time;
	public String departure_time;
	public String stop_id;
	public String stop_sequence;
	public String pickup_type;
	public String drop_off_type;

	public StopTimes() {
	}

	public StopTimes(String trip_id, String arrival_time,
			String departure_time, String stop_id, String stop_sequence,
			String pickup_type, String drop_off_type) {
		this.trip_id = trip_id;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
		this.stop_id = stop_id;
		this.stop_sequence = stop_sequence;
		this.pickup_type = pickup_type;
		this.drop_off_type = drop_off_type;
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}

	public String getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}

	public String getStop_id() {
		return stop_id;
	}

	public void setStop_id(String stop_id) {
		this.stop_id = stop_id;
	}

	public String getStop_sequence() {
		return stop_sequence;
	}

	public void setStop_sequence(String stop_sequence) {
		this.stop_sequence = stop_sequence;
	}

	public String getPickup_type() {
		return pickup_type;
	}

	public void setPickup_type(String pickup_type) {
		this.pickup_type = pickup_type;
	}

	public String getDrop_off_type() {
		return drop_off_type;
	}

	public void setDrop_off_type(String drop_off_type) {
		this.drop_off_type = drop_off_type;
	}

	@Override
	public String toString() {
		return "StopTimes{" + "trip_id='" + trip_id + '\'' + ", arrival_time='"
				+ arrival_time + '\'' + ", departure_time='" + departure_time
				+ '\'' + ", stop_id='" + stop_id + '\'' + ", stop_sequence='"
				+ stop_sequence + '\'' + ", pickup_type='" + pickup_type + '\''
				+ ", drop_off_type='" + drop_off_type + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		StopTimes stopTimes = (StopTimes) o;

		if (arrival_time != null ? !arrival_time.equals(stopTimes.arrival_time)
				: stopTimes.arrival_time != null) {
			return false;
		}
		if (departure_time != null ? !departure_time
				.equals(stopTimes.departure_time)
				: stopTimes.departure_time != null) {
			return false;
		}
		if (drop_off_type != null ? !drop_off_type
				.equals(stopTimes.drop_off_type)
				: stopTimes.drop_off_type != null) {
			return false;
		}
		if (pickup_type != null ? !pickup_type.equals(stopTimes.pickup_type)
				: stopTimes.pickup_type != null) {
			return false;
		}
		if (stop_id != null ? !stop_id.equals(stopTimes.stop_id)
				: stopTimes.stop_id != null) {
			return false;
		}
		if (stop_sequence != null ? !stop_sequence
				.equals(stopTimes.stop_sequence)
				: stopTimes.stop_sequence != null) {
			return false;
		}
		if (trip_id != null ? !trip_id.equals(stopTimes.trip_id)
				: stopTimes.trip_id != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = trip_id != null ? trip_id.hashCode() : 0;
		result = 31 * result
				+ (arrival_time != null ? arrival_time.hashCode() : 0);
		result = 31 * result
				+ (departure_time != null ? departure_time.hashCode() : 0);
		result = 31 * result + (stop_id != null ? stop_id.hashCode() : 0);
		result = 31 * result
				+ (stop_sequence != null ? stop_sequence.hashCode() : 0);
		result = 31 * result
				+ (pickup_type != null ? pickup_type.hashCode() : 0);
		result = 31 * result
				+ (drop_off_type != null ? drop_off_type.hashCode() : 0);
		return result;
	}
}
