package ro.krogan.brasovbus.models;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties({ "_id" })
public class Stop implements Comparable<Stop>, Parcelable {
	private static final long serialVersionUID = 9166090670181554150L;

	public String stop_id;

	public String stop_name;

	public Double stop_lat;

	public Double stop_lon;

	public Stop() {
	}

	public Stop(String stop_id, String stop_name, Double stop_lat,
			Double stop_lon) {
		this.stop_id = stop_id;
		this.stop_name = stop_name;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
	}

	public String getStop_id() {
		return stop_id;
	}

	public void setStop_id(String stop_id) {
		this.stop_id = stop_id;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	public Double getStop_lat() {
		return stop_lat;
	}

	public void setStop_lat(Double stop_lat) {
		this.stop_lat = stop_lat;
	}

	public Double getStop_lon() {
		return stop_lon;
	}

	public void setStop_lon(Double stop_lon) {
		this.stop_lon = stop_lon;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Stop stop = (Stop) o;

		if (stop_id != null ? !stop_id.equals(stop.stop_id)
				: stop.stop_id != null) {
			return false;
		}
		if (stop_lat != null ? !stop_lat.equals(stop.stop_lat)
				: stop.stop_lat != null) {
			return false;
		}
		if (stop_lon != null ? !stop_lon.equals(stop.stop_lon)
				: stop.stop_lon != null) {
			return false;
		}
		if (stop_name != null ? !stop_name.equals(stop.stop_name)
				: stop.stop_name != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = stop_id != null ? stop_id.hashCode() : 0;
		result = 31 * result + (stop_name != null ? stop_name.hashCode() : 0);
		result = 31 * result + (stop_lat != null ? stop_lat.hashCode() : 0);
		result = 31 * result + (stop_lon != null ? stop_lon.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Stop{" + "stop_id='" + stop_id + '\'' + ", stop_name='"
				+ stop_name + '\'' + ", stop_lat=" + stop_lat + ", stop_lon="
				+ stop_lon + '}';
	}

	@Override
	public int compareTo(Stop stop) {
		return stop_name.compareTo(stop.getStop_name());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(stop_id);
		parcel.writeString(stop_name);
		parcel.writeDouble(stop_lat);
		parcel.writeDouble(stop_lon);
	}

	public static final Parcelable.Creator<Stop> CREATOR = new Parcelable.Creator<Stop>() {
		public Stop createFromParcel(Parcel in) {
			return new Stop(in);
		}

		public Stop[] newArray(int size) {
			return new Stop[size];
		}
	};

	private Stop(Parcel in) {
		stop_id = in.readString();
		stop_name = in.readString();
		stop_lat = in.readDouble();
		stop_lon = in.readDouble();
	}
}
