package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RatTicketThirdPartyDist {
	private Integer nr;
	private String denumire;
	private String adresa;
	private String amplasament;
	private String observatii;

	public RatTicketThirdPartyDist() {
	}

	public RatTicketThirdPartyDist(Integer nr, String denumire, String adresa,
			String amplasament, String observatii) {
		this.nr = nr;
		this.denumire = denumire;
		this.adresa = adresa;
		this.amplasament = amplasament;
		this.observatii = observatii;
	}

	public Integer getNr() {
		return nr;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getAmplasament() {
		return amplasament;
	}

	public void setAmplasament(String amplasament) {
		this.amplasament = amplasament;
	}

	public String getObservatii() {
		return observatii;
	}

	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		RatTicketThirdPartyDist that = (RatTicketThirdPartyDist) o;

		if (adresa != null ? !adresa.equals(that.adresa) : that.adresa != null) {
			return false;
		}
		if (amplasament != null ? !amplasament.equals(that.amplasament)
				: that.amplasament != null) {
			return false;
		}
		if (denumire != null ? !denumire.equals(that.denumire)
				: that.denumire != null) {
			return false;
		}
		if (nr != null ? !nr.equals(that.nr) : that.nr != null) {
			return false;
		}
		if (observatii != null ? !observatii.equals(that.observatii)
				: that.observatii != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = nr != null ? nr.hashCode() : 0;
		result = 31 * result + (denumire != null ? denumire.hashCode() : 0);
		result = 31 * result + (adresa != null ? adresa.hashCode() : 0);
		result = 31 * result
				+ (amplasament != null ? amplasament.hashCode() : 0);
		result = 31 * result + (observatii != null ? observatii.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "RatTicketThirdPartyDist{" + "nr=" + nr + ", denumire='"
				+ denumire + '\'' + ", adresa='" + adresa + '\''
				+ ", amplasament='" + amplasament + '\'' + ", observatii='"
				+ observatii + '\'' + '}';
	}
}
