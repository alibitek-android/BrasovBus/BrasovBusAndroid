package ro.krogan.brasovbus.models;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
public class FareRules implements Serializable {
	private static final long serialVersionUID = -7016308268125419614L;
	public String fare_id;
	public String route_id;
	public String orgin_id;
}
