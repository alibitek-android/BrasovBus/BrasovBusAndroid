package ro.krogan.brasovbus.models;

import java.io.Serializable;

public class Favorite implements Serializable {

	private static final long serialVersionUID = 440965951503608420L;
	private Object obj;

	public Favorite() {
		
	}
	
	public Favorite(Object obj) {
		this.obj = obj;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public String toString() {
		if (obj instanceof Stop) {
			Stop stop = (Stop) obj;
			return stop.getStop_name() + " (" + "S" + ")";
		} else if (obj instanceof Route) {
			Route route = (Route) obj;
			return route.getRoute_desc() + " : " + route.getRoute_long_name()
					+ " (" + "R" + ")";
		}
		return null;
	}

	/*
	 * public boolean equals(Object o) { return o instanceof Favorite &&
	 * ((Favorite) o).obj.compareTo(obj) == 0; }
	 */
}