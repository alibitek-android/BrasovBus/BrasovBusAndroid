package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.math.BigDecimal;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tax {

	private int nr;

	private String denumire;

	private BigDecimal pret;

	private String valabilitate;

	private String descriere;

	public Tax() {
	}

	public Tax(int nr, String denumire, BigDecimal pret, String valabilitate,
			String descriere) {
		this.nr = nr;
		this.denumire = denumire;
		this.pret = pret;
		this.valabilitate = valabilitate;
		this.descriere = descriere;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public BigDecimal getPret() {
		return pret;
	}

	public void setPret(BigDecimal pret) {
		this.pret = pret;
	}

	public String getValabilitate() {
		return valabilitate;
	}

	public void setValabilitate(String valabilitate) {
		this.valabilitate = valabilitate;
	}

	public String getDescriere() {
		return descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Tax tax = (Tax) o;

		if (nr != tax.nr) {
			return false;
		}
		if (denumire != null ? !denumire.equals(tax.denumire)
				: tax.denumire != null) {
			return false;
		}
		if (descriere != null ? !descriere.equals(tax.descriere)
				: tax.descriere != null) {
			return false;
		}
		if (pret != null ? !pret.equals(tax.pret) : tax.pret != null) {
			return false;
		}
		if (valabilitate != null ? !valabilitate.equals(tax.valabilitate)
				: tax.valabilitate != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = nr;
		result = 31 * result + (denumire != null ? denumire.hashCode() : 0);
		result = 31 * result + (pret != null ? pret.hashCode() : 0);
		result = 31 * result
				+ (valabilitate != null ? valabilitate.hashCode() : 0);
		result = 31 * result + (descriere != null ? descriere.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Tax{" + "nr=" + nr + ", denumire='" + denumire + '\''
				+ ", pret=" + pret + ", valabilitate='" + valabilitate + '\''
				+ ", descriere='" + descriere + '\'' + '}';
	}
}
