package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entities {
	private List<MyUrl> urls;

	public List<MyUrl> getUrls() {
		return urls;
	}

	public void setUrls(List<MyUrl> urls) {
		this.urls = urls;
	}
}
