package ro.krogan.brasovbus.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RatTicketDist {
	private Integer nr;
	private String denumire;
	private String programlv;
	private String programsd;
	private String observatii;

	public RatTicketDist() {
	}

	public RatTicketDist(Integer nr, String denumire, String programlv,
			String programsd, String observatii) {
		this.nr = nr;
		this.denumire = denumire;
		this.programlv = programlv;
		this.programsd = programsd;
		this.observatii = observatii;
	}

	public Integer getNr() {
		return nr;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public String getProgramlv() {
		return programlv;
	}

	public void setProgramlv(String programlv) {
		this.programlv = programlv;
	}

	public String getProgramsd() {
		return programsd;
	}

	public void setProgramsd(String programsd) {
		this.programsd = programsd;
	}

	public String getObservatii() {
		return observatii;
	}

	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		RatTicketDist ratTicketDist = (RatTicketDist) o;

		if (denumire != null ? !denumire.equals(ratTicketDist.denumire)
				: ratTicketDist.denumire != null) {
			return false;
		}
		if (nr != null ? !nr.equals(ratTicketDist.nr)
				: ratTicketDist.nr != null) {
			return false;
		}
		if (observatii != null ? !observatii.equals(ratTicketDist.observatii)
				: ratTicketDist.observatii != null) {
			return false;
		}
		if (programlv != null ? !programlv.equals(ratTicketDist.programlv)
				: ratTicketDist.programlv != null) {
			return false;
		}
		if (programsd != null ? !programsd.equals(ratTicketDist.programsd)
				: ratTicketDist.programsd != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = nr != null ? nr.hashCode() : 0;
		result = 31 * result + (denumire != null ? denumire.hashCode() : 0);
		result = 31 * result + (programlv != null ? programlv.hashCode() : 0);
		result = 31 * result + (programsd != null ? programsd.hashCode() : 0);
		result = 31 * result + (observatii != null ? observatii.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "RatTicketDist{" + "nr=" + nr + ", denumire='" + denumire + '\''
				+ ", programlv='" + programlv + '\'' + ", programsd='"
				+ programsd + '\'' + ", observatii='" + observatii + '\'' + '}';
	}
}
