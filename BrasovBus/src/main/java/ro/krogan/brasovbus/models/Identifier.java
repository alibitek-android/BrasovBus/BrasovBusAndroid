package ro.krogan.brasovbus.models;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
public class Identifier {
	private long oid;

	public Identifier() {
	}

	public long getOid() {
		return oid;
	}

	public void setOid(long oid) {
		this.oid = oid;
	}
}
