package ro.krogan.brasovbus.models;

import java.io.Serializable;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 * 
 */
public class FareAttributes implements Serializable {
	private static final long serialVersionUID = -1229662955387117535L;
	public String fare_id;
	public String price;
	public String currency_type;
	public String payment_method;
	public String transfers;
	public String transfer_duration;
}
