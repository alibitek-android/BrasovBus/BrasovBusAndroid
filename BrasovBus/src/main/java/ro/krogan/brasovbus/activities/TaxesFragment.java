package ro.krogan.brasovbus.activities;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Tax;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.googlecode.androidannotations.annotations.EFragment;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EFragment(R.layout.taxes_fragment_layout)
public class TaxesFragment extends Fragment {

	WebView webView;

	private List<Tax> taxes;
	StringBuilder sb = new StringBuilder();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DownloadTaxesTask dnt = new DownloadTaxesTask();
		dnt.execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		return inflater.inflate(R.layout.taxes_fragment_layout, container,
				false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		webView = (WebView) getView().findViewById(R.id.taxesWebView);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
	}

	@Override
	public void onResume() {
		super.onResume();
		webView.loadData(sb.toString(), "text/html", "utf-8");
	}

	private class DownloadTaxesTask extends AsyncTask<String, Void, List<Tax>> {

		protected List<Tax> doInBackground(String... voids) {
			String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/tarife?apiKey=4fccc029e4b067a61b69c449";

			RestTemplate restTemplate = new RestTemplate();

			restTemplate.getMessageConverters().add(
					new MappingJacksonHttpMessageConverter());

			Tax[] taxes = restTemplate.getForObject(url, Tax[].class);
			return Arrays.asList(taxes);
		}

		protected void onPostExecute(List<Tax> result) {
			taxes = result;
			sb.append("<html>");
			sb.append("<body>");
			sb.append("<p><strong>Tarife Regia de Transport BRASOV</strong></p>");
			sb.append("<p>Legitimatiile de calatorie valabile pe traseele RAT BRASOV se pot procura de la tonetele speciale amplasate in statiile principale sau de la distribuitori.</p>");
			sb.append("<table width=\"98%\" border=1 cellpadding=2 cellspacing=2>");
			sb.append("<th bgcolor=\"#f7cb33\">Nr</th><th bgcolor=\"#f7cb33\">Denumire</th><th bgcolor=\"#f7cb33\">Pret</th><th bgcolor=\"#f7cb33\">Valabilitate</th><th bgcolor=\"#f7cb33\">Descriere</th>");
			for (Tax t : taxes) {
				sb.append("<tr>");
				sb.append("<td>").append(t.getNr()).append("</td>");
				sb.append("<td>").append(t.getDenumire()).append("</td>");
				sb.append("<td>").append(t.getPret()).append("</td>");
				sb.append("<td>").append(t.getValabilitate()).append("</td>");
				sb.append("<td>").append(t.getDescriere()).append("</td>");
				sb.append("<tr>");
			}
			sb.append("</table>");
			sb.append("</body>");
			sb.append("</html>");

			webView.loadData(sb.toString(), "text/html", "utf-8");
		}
	}
}
