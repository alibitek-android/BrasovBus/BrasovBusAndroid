package ro.krogan.brasovbus.activities;

import java.util.ArrayList;
import java.util.List;

import ro.krogan.brasovbus.BrasovBusApp;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Favorite;
import ro.krogan.brasovbus.models.Route;
import android.app.ListActivity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.NonConfigurationInstance;

import de.akquinet.android.androlog.Log;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class RoutesActivity extends ListActivity {

	private static final String TAG = RoutesActivity.class.getSimpleName();

	@NonConfigurationInstance
	@Bean
	MyBackgroundTask task;

	@App
	BrasovBusApp application;

	private List<Route> routeList;

	private final Object mLock = new Object();
	private boolean inversed = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.routes_layout);

		task.routesActivityBackground();

		getListView().setTextFilterEnabled(true);
		registerForContextMenu(getListView());
		
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				/*Log.d(TAG, "Clicked : " + route.getRoute_long_name());*/
				Intent intent = new Intent(getBaseContext(), RouteDetailsActivity_.class);
				intent.putExtra("route", routeList.get(position));
				startActivity(intent);
			}			
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(routeList.get(info.position).getRoute_long_name());

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.routes_context_menu, menu);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		Route route = (Route) getListAdapter().getItem(info.position);

		switch (item.getItemId()) {
		case R.id.viewDirectRoute: {
			Log.d(TAG, "Clicked : " + route.getRoute_long_name());
			Intent intent = new Intent(this, RouteDetailsActivity_.class);
			intent.putExtra("route", route);
			startActivity(intent);
			return true;
		}
		case R.id.viewInverseRoute: {
			inverse(route);

			Log.d(TAG, "Clicked : " + route.getRoute_long_name());
			Intent intent = new Intent(this, RouteDetailsActivity_.class);
			intent.putExtra("route", route);
			startActivity(intent);
			return true;
		}
		case R.id.addRouteToFavorites: {
			application.favorites.add(new Favorite(route));
			return true;
		}
		case R.id.viewScheduleForDirectRoute: {
			inversed = false;
			Intent intent = new Intent(this, ScheduleActivity_.class);
			intent.putExtra("route", route);
			startActivity(intent);
		}
		case R.id.viewScheduleForInverseRoute: {
			inversed = true;
			inverse(route);
			Intent intent = new Intent(this, ScheduleActivity_.class);
			intent.putExtra("route", route);
			intent.putExtra("inversed", inversed);
			startActivity(intent);
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	@SuppressWarnings("unchecked")
	private void inverse(Route route) {
		String[] routeLongName = route.getRoute_long_name().split("-");

		if (routeLongName.length == 2)
			route.setRoute_long_name(routeLongName[1].trim() + " - "
					+ routeLongName[0].trim());
		else if (routeLongName.length == 3) {
			route.setRoute_long_name(routeLongName[2].trim() + " - "
					+ routeLongName[1].trim() + " - "
					+ routeLongName[0].trim());
		} else if (routeLongName.length == 4) {
			route.setRoute_long_name(routeLongName[3].trim() + " - "
					+ routeLongName[2].trim() + "- "
					+ routeLongName[1].trim() + " - " + routeLongName[0]);
		}

		((ArrayAdapter<Route>) getListView().getAdapter())
				.notifyDataSetChanged();
	}
	
	public void setRouteList(List<Route> routes) {
		routeList = routes;
		setListAdapter(new RoutesAdapter());
	}

	private class RoutesAdapter extends ArrayAdapter<Route> {

		private RouteFilter mFilter;

		private ArrayList<Route> mOriginalValues;

		public RoutesAdapter() {
			super(RoutesActivity.this, R.layout.route_row, R.id.title,
					routeList);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.route_row, parent, false);

			TextView title = (TextView) row.findViewById(R.id.title);
			TextView artist = (TextView) row.findViewById(R.id.artist);

			TextView duration = (TextView) row.findViewById(R.id.duration);
			ImageView thumb_image = (ImageView) row
					.findViewById(R.id.list_image);

			Route route = routeList.get(position);
			title.setText(route.getRoute_long_name());
			artist.setText(route.getRoute_desc());
			duration.setText(route.getRoute_short_name());

			if (route.getRoute_id().equals("1")) {
				thumb_image.setImageResource(R.drawable.unu);
			}
			if (route.getRoute_id().equals("2")) {
				thumb_image.setImageResource(R.drawable.doi);
			}
			if (route.getRoute_id().equals("3")) {
				thumb_image.setImageResource(R.drawable.trei);
			}
			if (route.getRoute_id().equals("4")) {
				thumb_image.setImageResource(R.drawable.patru);
			}
			if (route.getRoute_id().equals("5")) {
				thumb_image.setImageResource(R.drawable.cinci);
			}
			if (route.getRoute_id().equals("5b")) {
				thumb_image.setImageResource(R.drawable.cincib);
			}
			if (route.getRoute_id().equals("6")) {
				thumb_image.setImageResource(R.drawable.sase);
			}
			if (route.getRoute_id().equals("7")) {
				thumb_image.setImageResource(R.drawable.sapte);
			}
			if (route.getRoute_id().equals("8")) {
				thumb_image.setImageResource(R.drawable.opt);
			}
			if (route.getRoute_id().equals("9b")) {
				thumb_image.setImageResource(R.drawable.nouab);
			}
			if (route.getRoute_id().equals("10")) {
				thumb_image.setImageResource(R.drawable.zece);
			}
			if (route.getRoute_id().equals("11")) {
				thumb_image.setImageResource(R.drawable.unspe);
			}
			if (route.getRoute_id().equals("12")) {
				thumb_image.setImageResource(R.drawable.doispe);
			}
			if (route.getRoute_id().equals("14")) {
				thumb_image.setImageResource(R.drawable.paispe);
			}
			if (route.getRoute_id().equals("16")) {
				thumb_image.setImageResource(R.drawable.saispe);
			}
			if (route.getRoute_id().equals("17")) {
				thumb_image.setImageResource(R.drawable.saptispe);
			}
			if (route.getRoute_id().equals("17b")) {
				thumb_image.setImageResource(R.drawable.saptispeb);
			}
			if (route.getRoute_id().equals("18")) {
				thumb_image.setImageResource(R.drawable.optispe);
			}
			if (route.getRoute_id().equals("19")) {
				thumb_image.setImageResource(R.drawable.nouaspe);
			}
			if (route.getRoute_id().equals("20")) {
				thumb_image.setImageResource(R.drawable.douazeci);
			}
			if (route.getRoute_id().equals("21")) {
				thumb_image.setImageResource(R.drawable.douazeciunu);
			}
			if (route.getRoute_id().equals("22")) {
				thumb_image.setImageResource(R.drawable.douzecidoi);
			}
			if (route.getRoute_id().equals("23")) {
				thumb_image.setImageResource(R.drawable.douazecitrei);
			}
			if (route.getRoute_id().equals("23b")) {
				thumb_image.setImageResource(R.drawable.douazecitreib);
			}
			if (route.getRoute_id().equals("24")) {
				thumb_image.setImageResource(R.drawable.douazecipatru);
			}
			if (route.getRoute_id().equals("25")) {
				thumb_image.setImageResource(R.drawable.douazecicinci);
			}
			if (route.getRoute_id().equals("26")) {
				thumb_image.setImageResource(R.drawable.douazecisase);
			}
			if (route.getRoute_id().equals("28")) {
				thumb_image.setImageResource(R.drawable.douazeciopt);
			}
			if (route.getRoute_id().equals("28b")) {
				thumb_image.setImageResource(R.drawable.douazecioptb);
			}
			if (route.getRoute_id().equals("30")) {
				thumb_image.setImageResource(R.drawable.treizeci);
			}
			if (route.getRoute_id().equals("31")) {
				thumb_image.setImageResource(R.drawable.treizeciunu);
			}
			if (route.getRoute_id().equals("32")) {
				thumb_image.setImageResource(R.drawable.treizecidoi);
			}
			if (route.getRoute_id().equals("33")) {
				thumb_image.setImageResource(R.drawable.treizecitrei);
			}
			if (route.getRoute_id().equals("34")) {
				thumb_image.setImageResource(R.drawable.treizecipatru);
			}
			if (route.getRoute_id().equals("34b")) {
				thumb_image.setImageResource(R.drawable.treizecipatrub);
			}
			if (route.getRoute_id().equals("35")) {
				thumb_image.setImageResource(R.drawable.treizecicinci);
			}
			if (route.getRoute_id().equals("36")) {
				thumb_image.setImageResource(R.drawable.treizecisase);
			}
			if (route.getRoute_id().equals("37")) {
				thumb_image.setImageResource(R.drawable.treizecisapte);
			}
			if (route.getRoute_id().equals("40")) {
				thumb_image.setImageResource(R.drawable.patruzeci);
			}
			if (route.getRoute_id().equals("41")) {
				thumb_image.setImageResource(R.drawable.patruzeciunu);
			}
			if (route.getRoute_id().equals("42")) {
				thumb_image.setImageResource(R.drawable.patruzecidoi);
			}
			if (route.getRoute_id().equals("44")) {
				thumb_image.setImageResource(R.drawable.patruzecipatru);
			}
			if (route.getRoute_id().equals("45")) {
				thumb_image.setImageResource(R.drawable.patruzecicinci);
			}
			if (route.getRoute_id().equals("50")) {
				thumb_image.setImageResource(R.drawable.cincizeci);
			}
			if (route.getRoute_id().equals("50b")) {
				thumb_image.setImageResource(R.drawable.cincizecib);
			}
			if (route.getRoute_id().equals("51")) {
				thumb_image.setImageResource(R.drawable.cincizeciunu);
			}
			if (route.getRoute_id().equals("52")) {
				thumb_image.setImageResource(R.drawable.cincizecidoi);
			}

			return row;
		}

		@Override
		public Route getItem(int position) {
			return routeList.get(position);
		}

		@Override
		public int getCount() {
			return routeList.size();
		}

		@Override
		public Filter getFilter() {
			if (mFilter == null) {
				mFilter = new RouteFilter();
			}
			return mFilter;
		}

		private class RouteFilter extends Filter {

			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				if (mOriginalValues == null) {
					synchronized (mLock) {
						mOriginalValues = new ArrayList<Route>(routeList);
					}
				}

				if (charSequence == null || charSequence.length() == 0) {
					ArrayList<Route> list;
					synchronized (mLock) {
						list = new ArrayList<Route>(mOriginalValues);
					}
					results.values = list;
					results.count = list.size();
				} else {
					String prefixString = charSequence.toString().toLowerCase();

					ArrayList<Route> values;
					synchronized (mLock) {
						values = new ArrayList<Route>(mOriginalValues);
					}

					final int count = values.size();
					final ArrayList<Route> newValues = new ArrayList<Route>();

					for (int i = 0; i < count; i++) {
						final Route value = values.get(i);

						final String valueText = value.getRoute_long_name()
								.toLowerCase();

						if (valueText.contains(prefixString.toLowerCase())) {
							newValues.add(value);
						} else {
							final String[] words = valueText.split(" ");
							final int wordCount = words.length;

							for (int k = 0; k < wordCount; k++) {
								if (words[k].toLowerCase().contains(
										prefixString.toLowerCase())) {
									newValues.add(value);
									break;
								}
							}
						}
					}

					results.values = newValues;
					results.count = newValues.size();
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				routeList = (List<Route>) filterResults.values;
				if (filterResults.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.routes_menu, menu);

		SearchView searchView = (SearchView) menu.findItem(R.id.searchRoute)
				.getActionView();

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		ComponentName cn = new ComponentName(this, RoutesActivity_.class);
		SearchableInfo info = searchManager.getSearchableInfo(cn);
		searchView.setSearchableInfo(info);

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@SuppressWarnings("unchecked")
			@Override
			public boolean onQueryTextSubmit(String s) {
				ArrayAdapter<Route> adapter = (ArrayAdapter<Route>) getListAdapter();
				adapter.getFilter().filter(s.toLowerCase());
				return true;
			}

			@SuppressWarnings("unchecked")
			@Override
			public boolean onQueryTextChange(String s) {
				ArrayAdapter<Route> adapter = (ArrayAdapter<Route>) getListAdapter();
				adapter.getFilter().filter(s.toLowerCase());
				return true;
			}
		});

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.icon:
			final Intent intent = new Intent(this, MainActivity_.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
