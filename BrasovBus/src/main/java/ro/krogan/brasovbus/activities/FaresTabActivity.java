package ro.krogan.brasovbus.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TabHost;
import com.googlecode.androidannotations.annotations.EActivity;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.utils.TabManager;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class FaresTabActivity extends FragmentActivity {

	TabHost mTabHost;

	TabManager mTabManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fares_layout);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		mTabManager = new TabManager(this, mTabHost, R.id.farestabcontent);

		mTabManager.addTab(mTabHost.newTabSpec("tarife").setIndicator("Fare"),
				TaxesFragment.class, null);
		mTabManager.addTab(
				mTabHost.newTabSpec("bileterat").setIndicator(
						"Tickets - R.A.T."), TicketsRatFragment.class, null);
		mTabManager.addTab(
				mTabHost.newTabSpec("bileteterti").setIndicator(
						"Tickets - Third Party"),
				TicketsThirdPartyFragment.class, null);
		mTabManager
				.addTab(mTabHost.newTabSpec("buytickets").setIndicator(
						"Buy - Tickets"), BuyTicketsFragment.class, null);

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}
}
