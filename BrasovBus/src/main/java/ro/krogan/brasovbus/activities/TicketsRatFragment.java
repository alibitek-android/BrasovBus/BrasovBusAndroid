package ro.krogan.brasovbus.activities;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.RatTicketDist;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.googlecode.androidannotations.annotations.EFragment;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EFragment
public class TicketsRatFragment extends Fragment {

	WebView webView;

	private List<RatTicketDist> ratDistTicketDist;
	StringBuilder sb = new StringBuilder();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DownloadTicketsRatTask dnt = new DownloadTicketsRatTask();
		dnt.execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		return inflater.inflate(R.layout.ticketsrat_fragment_layout, container,
				false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		webView = (WebView) getView().findViewById(R.id.ticketRatWebView);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
	}

	@Override
	public void onResume() {
		super.onResume();
		webView.loadData(sb.toString(), "text/html", "utf-8");
	}

	private class DownloadTicketsRatTask extends
			AsyncTask<String, Void, List<RatTicketDist>> {

		protected List<RatTicketDist> doInBackground(String... voids) {
			String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/bilete_rat?apiKey=4fccc029e4b067a61b69c449";
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(
					new MappingJacksonHttpMessageConverter());
			RatTicketDist[] taxes = restTemplate.getForObject(url,
					RatTicketDist[].class);
			return Arrays.asList(taxes);
		}

		protected void onPostExecute(List<RatTicketDist> result) {
			ratDistTicketDist = result;
			sb.append("<html>");
			sb.append("<body>");
			sb.append("<p><strong>Tarife Regia de Transport BRASOV</strong></p>");
			sb.append("<p>Legitimatiile de calatorie valabile pe traseele RAT BRASOV se pot procura de la tonetele speciale amplasate in statiile principale sau de la distribuitori.</p>");
			sb.append("<p><b>Reteaua R.A.T. Brasov de distributie a biletelor si abonamentelor</b></p>");
			sb.append("<table width=\"98%\" border=1 cellpadding=2 cellspacing=2>");
			sb.append("<th bgcolor=\"#f7cb33\">Nr</th><th bgcolor=\"#f7cb33\">Denumire</th><th bgcolor=\"#f7cb33\">Program L-V</th><th bgcolor=\"#f7cb33\">Program S-D</th><th bgcolor=\"#f7cb33\">Observatii</th>");
			for (RatTicketDist t : ratDistTicketDist) {
				sb.append("<tr>");
				sb.append("<td>").append(t.getNr()).append("</td>");
				sb.append("<td>").append(t.getDenumire()).append("</td>");
				sb.append("<td>");
				if (t.getProgramlv() != null && t.getProgramlv().contains(";")) {
					String[] shd = t.getProgramlv().split(";");
					sb.append(shd[0].trim()).append("</br>");
					sb.append(shd[1].trim());
				}
				sb.append("</td>");
				sb.append("<td>");
				if (t.getProgramsd() != null && t.getProgramsd().contains(";")) {
					String[] shd = t.getProgramsd().split(";");
					sb.append(shd[0].trim()).append("</br>");
					sb.append(shd[1].trim());
				}
				sb.append("</td>");
				sb.append("<td>").append(t.getObservatii()).append("</td>");
				sb.append("<tr>");
			}
			sb.append("</table>");
			sb.append("*In incinta acestor statii sunt chioscuri de ziare care vand bilete.</br>");
			sb.append("*Cabinele R.A.T isi modifica programul de functionare in cazul in care chioscurile de ziare sunt inchise.</br>");
			sb.append("</body>");
			sb.append("</html>");

			webView.loadData(sb.toString(), "text/html", "utf-8");
		}
	}
}
