package ro.krogan.brasovbus.activities;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Stop;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ViewById;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity(R.layout.stops_details_layout)
public class StopsDetailsActivity extends Activity {

	@ViewById
	TextView stopNameTextView;

	@Extra
	Stop stop;

	@AfterViews
	void after() {
		stopNameTextView.setText(stop.getStop_name());
	}

	@Click
	void showStopOnMap() {
		Intent mapIntent = new Intent(this, MapsActivity_.class);
		mapIntent.putExtra("stop", (Parcelable) stop);
		startActivity(mapIntent);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, StopsActivity_.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);            
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
}
