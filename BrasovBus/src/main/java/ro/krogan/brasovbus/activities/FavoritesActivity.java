package ro.krogan.brasovbus.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import ro.krogan.brasovbus.BrasovBusApp;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Favorite;
import ro.krogan.brasovbus.models.Route;
import ro.krogan.brasovbus.models.Stop;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class FavoritesActivity extends ListActivity {

	@App
	BrasovBusApp application;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);	
		
		setContentView(R.layout.favorites_layout);			

		setListAdapter(new ArrayAdapter<Favorite>(this,
				android.R.layout.simple_list_item_1, application.favorites));
		registerForContextMenu(getListView());
	}

	@ItemClick
	void listItemClicked(Favorite fav) {
		Intent i = null;
		if (fav.getObj() instanceof Stop) {
			Stop stop = (Stop) fav.getObj();
			Toast.makeText(this, "click: " + stop.getStop_name(),
					Toast.LENGTH_SHORT).show();
			i = new Intent(this, StopsDetailsActivity_.class);
			i.putExtra("stop", (Parcelable) stop);
		} else if (fav.getObj() instanceof Route) {
			Route route = (Route) fav.getObj();
			Toast.makeText(this, "click: " + route.getRoute_long_name(),
					Toast.LENGTH_SHORT).show();
			i = new Intent(this, RouteDetailsActivity_.class);
			i.putExtra("route", route);
		}

		startActivity(i);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		menu.setHeaderTitle("Favorites Context Menu");

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.favorites_context_menu, menu);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onContextItemSelected(MenuItem aItem) {
		AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) aItem
				.getMenuInfo();

		switch (aItem.getItemId()) {
		case R.id.favdelete:
			Favorite favContexted = (Favorite) getListView().getAdapter()
					.getItem(menuInfo.position);

			application.favorites.remove(favContexted);

			ArrayAdapter<Favorite> aa = (ArrayAdapter<Favorite>) getListAdapter();
			aa.notifyDataSetChanged();
			return true;
		}
		return false;
	}	

	public boolean saveArray(String[] array, String arrayName, Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences("favorites", 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(arrayName + "_size", array.length);
		for (int i = 0; i < array.length; i++)
			editor.putString(arrayName + "_" + i, array[i]);
		return editor.commit();
	}

	public String[] loadArray(String arrayName, Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences("favorites", 0);
		int size = prefs.getInt(arrayName + "_size", 0);
		String array[] = new String[size];
		for (int i = 0; i < size; i++)
			array[i] = prefs.getString(arrayName + "_" + i, null);
		return array;
	}
}
