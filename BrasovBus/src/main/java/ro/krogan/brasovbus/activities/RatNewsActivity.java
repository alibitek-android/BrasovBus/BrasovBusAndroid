package ro.krogan.brasovbus.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.News;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
public class RatNewsActivity extends FragmentActivity {

	private static List<News> newsList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ratnews_fragment_layout);
	}

	public static class NewsTitlesFragment extends ListFragment {

		boolean mDualPane;

		int mCurCheckPosition = 0;

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			DownloadNewsTask dnt = new DownloadNewsTask();
			dnt.execute();
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			View detailsFrame = getActivity().findViewById(R.id.details);
			mDualPane = detailsFrame != null
					&& detailsFrame.getVisibility() == View.VISIBLE;

			if (savedInstanceState != null) {
				mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
			}

			if (mDualPane) {
				getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				showDetails(mCurCheckPosition);
			}
		}

		private class DownloadNewsTask extends
				AsyncTask<String, Void, List<News>> {

			protected List<News> doInBackground(String... urls) {
				String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/news?apiKey=4fccc029e4b067a61b69c449";

				RestTemplate restTemplate = new RestTemplate();

				restTemplate.getMessageConverters().add(
						new MappingJacksonHttpMessageConverter());

				News[] news = restTemplate.getForObject(url, News[].class);
				return Arrays.asList(news);
			}

			protected void onPostExecute(List<News> result) {
				newsList = result;
				Collections.sort(newsList, new Comparator<News>() {
					@Override
					public int compare(News n1, News n2) {
						Date d1 = new Date(n1.getDate());
						Date d2 = new Date(n2.getDate());

						return -d1.compareTo(d2);
					}
				});

				setListAdapter(new NewsAdapter());
			}
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
			outState.putInt("curChoice", mCurCheckPosition);
		}

		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			showDetails(position);
		}

		void showDetails(int index) {
			mCurCheckPosition = index;

			if (mDualPane) {

				getListView().setItemChecked(index, true);

				DetailsFragment details = (DetailsFragment) getFragmentManager()
						.findFragmentById(R.id.details);
				if (details == null || details.getShownIndex() != index) {
					details = DetailsFragment.newInstance(index);

					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					ft.replace(R.id.details, details);
					ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					ft.commit();
				}
			} else {
				Intent intent = new Intent();
				intent.setClass(getActivity(), RatNewsDetailsActivity.class);
				intent.putExtra("index", index);
				startActivity(intent);
			}
		}

		private class NewsAdapter extends ArrayAdapter<News> {

			public NewsAdapter() {
				super(getActivity(), R.layout.ratbvnews_row, R.id.title,
						newsList);
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = getActivity().getLayoutInflater();
				View row = inflater.inflate(R.layout.ratbvnews_row, parent,
						false);

				TextView title = (TextView) row.findViewById(R.id.title);
				TextView artist = (TextView) row.findViewById(R.id.artist);

				TextView duration = (TextView) row.findViewById(R.id.duration);

				News nws = newsList.get(position);
				title.setText(nws.getTitle());
				artist.setText(nws.getSubtitle());
				Date date = new Date(nws.getDate());
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
				duration.setText(sdf.format(date));

				return row;
			}
		}
	}

	public static class DetailsFragment extends Fragment {

		public static DetailsFragment newInstance(int index) {
			DetailsFragment f = new DetailsFragment();

			Bundle args = new Bundle();
			args.putInt("index", index);
			f.setArguments(args);

			return f;
		}

		public int getShownIndex() {
			return getArguments().getInt("index", 0);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			if (container == null) {
				return null;
			}

			ScrollView scroller = new ScrollView(getActivity());
			scroller.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.woodtexture));
			TextView text = new TextView(getActivity());
			int padding = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 4, getActivity()
							.getResources().getDisplayMetrics());
			text.setPadding(padding, padding, padding, padding);
			scroller.addView(text);
			News n1 = newsList.get(getShownIndex());
			DateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
			Date d = new Date(n1.getDate());
			text.setText("Title: " + n1.getTitle() + "\n" + "Subtitle: "
					+ n1.getSubtitle() + "\n" + "Date: " + sdf.format(d) + "\n"
					+ "Contents: " + n1.getContents());
			return scroller;
		}
	}

	public static class RatNewsDetailsActivity extends FragmentActivity {

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				finish();
				return;
			}

			if (savedInstanceState == null) {
				DetailsFragment details = new DetailsFragment();
				details.setArguments(getIntent().getExtras());
				getSupportFragmentManager().beginTransaction()
						.add(android.R.id.content, details).commit();
			}
		}
	}
}
