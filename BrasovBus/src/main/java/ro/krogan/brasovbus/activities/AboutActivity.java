package ro.krogan.brasovbus.activities;

import android.app.Activity;
import android.os.Bundle;
import com.googlecode.androidannotations.annotations.EActivity;
import ro.krogan.brasovbus.R;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity(R.layout.about_layout)
public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
	}
}
