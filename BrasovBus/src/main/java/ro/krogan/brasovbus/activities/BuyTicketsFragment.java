package ro.krogan.brasovbus.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.googlecode.androidannotations.annotations.EFragment;
import ro.krogan.brasovbus.R;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EFragment
public class BuyTicketsFragment extends Fragment {

	TextView mainText;

	Button buttonSend;

	private static final String TAG = BuyTicketsFragment.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		return inflater.inflate(R.layout.buy_tickets_layout, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mainText = (TextView) getActivity().findViewById(R.id.mainTextView);
		mainText.setText(Html.fromHtml(getActivity().getString(
				R.string.main_text_html)));

		buttonSend = (Button) getActivity().findViewById(R.id.btnPayBySms);

		buttonSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					/*
					 * TelephonyManager tMgr
					 * =(TelephonyManager)getActivity().getSystemService
					 * (Context.TELEPHONY_SERVICE); String phoneNumber =
					 * tMgr.getLine1Number();
					 */
					Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri
							.parse("smsto:+40765625471"));
					sendIntent.putExtra("sms_body", "PAY1DAY");
					// sendIntent.setType("vnd.android-dir/mms-sms");
					startActivity(sendIntent);
				} catch (Exception e) {
					Toast.makeText(getActivity().getApplicationContext(),
							"SMS faild, please try again later!",
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
			}
		});
	}
}
