package ro.krogan.brasovbus.activities;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Route;
import ro.krogan.brasovbus.models.Stop;
import ro.krogan.brasovbus.models.StopTimes;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

import de.akquinet.android.androlog.Log;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity(R.layout.route_details)
public class RouteDetailsActivity extends Activity {

	@ViewById(R.id.routeName)
	TextView routeName;

	@ViewById(R.id.numberOfStops)
	TextView numberOfStops;

	@ViewById(R.id.routeDetailsLinearLayout)
	LinearLayout linearLayout;

	Route route;

	Map<Route, List<Stop>> routeStops = new LinkedHashMap<Route, List<Stop>>();
	private ProgressDialog progressDialog;
	private boolean destroyed = false;

	private static final String TAG = RouteDetailsActivity.class
			.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		route = (Route) intent.getExtras().get("route");

		DownloadRouteStopsTask dst = new DownloadRouteStopsTask();
		dst.execute();
	}

	private class DownloadRouteStopsTask extends
			AsyncTask<Void, Void, List<Stop>> {

		@Override
		protected void onPreExecute() {
			showLoadingProgressDialog();
		}

		// {"trip_id":"1/WEE_1/0/0"}&l=12&f={"_id":0,"pickup_type":0,"drop_off_type":0}
		protected List<Stop> doInBackground(Void... urls) {
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(
					new MappingJacksonHttpMessageConverter());

			String baseUrl = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/stop_times?apiKey=4fccc029e4b067a61b69c449&q=";

			String startStop = null;
			String endStop = null;
			//Log.i(TAG, route.getRoute_long_name());
			if (route.getRoute_long_name().split("-").length == 2) { // 0 1
				startStop = route.getRoute_long_name().split("-")[0].trim()
						.toUpperCase();
				endStop = route.getRoute_long_name().split("-")[1].trim()
						.toUpperCase();
			} else if (route.getRoute_long_name().split("-").length == 3) { // 0
																			// 1
																			// 2
				startStop = route.getRoute_long_name().split("-")[0].trim()
						.toUpperCase();
				endStop = route.getRoute_long_name().split("-")[2].trim()
						.toUpperCase();
			} else if (route.getRoute_long_name().split("-").length == 4) { // 0
																			// 1
																			// 2
																			// 3
				startStop = route.getRoute_long_name().split("-")[0].trim()
						.toUpperCase();
				endStop = route.getRoute_long_name().split("-")[3].trim()
						.toUpperCase();
			}

			String firstQueryUrl = null;

			//Log.i(TAG, "StartStop :=> " + startStop);
			//Log.i(TAG, "EndStop :=> " + endStop);

			// -----------------------------------------------------------------------
			// L1
			// Triaj - Livada Postei 1/WEE_1/0/0,05:20:00,05:20:00,TRIAJ,1,0,
			// Livada Postei - Triaj 1/WEE_1/1/0,05:30:00,05:30:00,LIVADA,1,0,
			assert startStop != null;
			assert endStop != null;

			if (startStop.startsWith("TRIAJ") && endStop.startsWith("LIVADA")) {
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"1/WEE_1/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			} else if (startStop.startsWith("LIVADA")
					&& endStop.startsWith("TRIAJ")) {
				startStop = "LIVADA";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"1/WEE_1/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L2
			// Rulmentul - Livada Postei
			// 2/WEE_2/0/0,05:34:00,05:34:00,RULMENTUL_CAPAT,1,0,
			// Livada Postei - Rulmentul
			// 2/WEE_2/1/0,05:35:00,05:35:00,LIVADA,1,0,
			if (startStop.startsWith("RULMENTUL")
					&& endStop.startsWith("LIVADA")) {
				startStop = "RULMENTUL_CAPAT";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"2/WEE_2/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("LIVADA")
					&& endStop.startsWith("RULMENTUL")) {
				startStop = "LIVADA";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"2/WEE_2/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L3
			// Valea Cetatii - Stadionul Tineretului
			// 3/WEE_3/0/0,05:55:00,05:55:00,VALEA_CETATII_CAP,1,0,
			// Stadionul Tineretului - Valea Cetatii
			// 3/WEE_3/1/0,06:22:00,06:22:00,STADION_TIN_GRIV,1,0,
			if (startStop.startsWith("VALEA")
					&& endStop.startsWith("STADIONUL TINERETULUI")) {
				startStop = "VALEA_CETATII_CAP";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"3/WEE_3/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("STADIONUL TINERETULUI")
					&& endStop.startsWith("VALEA")) {
				startStop = "STADION_TIN_GRIV";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"3/WEE_3/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L4
			// Gara - Livada 4/WEE_4/1/0,05:35:00,05:35:00,GARA,1,0,
			// Livada - Gara 4/WEE_4/0/0,05:29:00,05:29:00,LIVADA,1,0,
			if (startStop.startsWith("LIVADA")
					&& endStop.startsWith("GARA BRASOV")) {
				startStop = "LIVADA";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"4/WEE_4/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("GARA") && endStop.startsWith("LIVADA")) {
				startStop = "GARA";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"4/WEE_4/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L5
			// Roman - Municipal 5/WEE_5/0/0,05:24:00,05:24:00,ROMAN,1,0,
			// Municipal - Roman 5/WEE_5/1/0,05:23:00,05:23:00,MUNICIPAL,1,0,
			if (startStop.startsWith("ROMAN")
					&& endStop.startsWith("STADIONUL MUNICIPAL")) {
				startStop = "ROMAN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"5/WEE_5/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("STADIONUL MUNICIPAL")
					&& endStop.startsWith("ROMAN")) {
				startStop = "MUNICIPAL";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"5/WEE_5/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L5b
			// Roman - Stadionului Tineretului
			// 5b/WEE_5b/0/0,05:30:00,05:30:00,ROMAN,1,0,
			// Stadionul Tineretului - Roman
			// 5b/WEE_5b/1/10,16:07:00,16:07:00,STADION_TIN_GRIV,1,0,
			if (startStop.startsWith("ROMAN")
					&& endStop.startsWith("STADIONUL TINERETULUI")) {
				startStop = "ROMAN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"5b/WEE_5b/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("STADIONUL TINERETULUI")
					&& endStop.startsWith("ROMAN")) {
				startStop = "STADION_TIN_GRIV";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"5b/WEE_5b/1/10\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L6
			// Livada - Saturn 6/WEE_6/0/0,05:41:00,05:41:00,LIVADA,1,0,
			// Saturn - Livada 6/WEE_6/1/0,05:35:00,05:35:00,SATURN,1,0,
			if (startStop.startsWith("LIVADA") && endStop.startsWith("SATURN")) {
				startStop = "ROMAN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"6/WEE_6/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("SATURN") && endStop.startsWith("LIVADA")) {
				startStop = "SATURN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"6/WEE_6/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L7
			// Roman - Rulmentul 7/WEE_7/0/0,05:45:00,05:45:00,ROMAN,1,0,
			// Rulmentul - Roman
			// 7/WEE_7/1/0,05:47:00,05:47:00,RULMENTUL_CAPAT,1,0,
			if (startStop.startsWith("ROMAN")
					&& endStop.startsWith("RULMENTUL")) {
				startStop = "ROMAN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"7/WEE_7/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("RULMENTUL")
					&& endStop.startsWith("ROMAN")) {
				startStop = "RULMENTUL_CAPAT";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"7/WEE_7/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L8
			// Saturn - Rulmentul 8/WEE_8/0/0,05:25:00,05:25:00,SATURN,1,0,
			// Rulmentul - Saturn
			// 8/WEE_8/1/0,05:29:00,05:29:00,RULMENTUL_CAPAT,1,0,
			if (startStop.startsWith("SATURN")
					&& endStop.startsWith("RULMENTUL")) {
				startStop = "SATURN";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"8/WEE_8/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("RULMENTUL")
					&& endStop.startsWith("SATURN")) {
				startStop = "RULMENTUL_CAPAT";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\"8/WEE_8/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L9b
			// Rulmentul - Bartolomeu Nord
			// 9b/WEE_9b/0/0,05:35:00,05:35:00,RULMENTUL_CAPAT,1,0,
			// Bartolomeu Nord - Rulmentul
			// 9b/WEE_9b/1/0,05:34:00,05:34:00,BARTOLO_NORD_CAP,1,0,
			if (startStop.startsWith("RULMENTUL")
					&& endStop.startsWith("BARTOLOMEU")) {
				startStop = "RULMENTUL_CAPAT";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\""
						+ route.getRoute_id()
						+ "/WEE_"
						+ route.getRoute_id()
						+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("BARTOLOMEU")
					&& endStop.startsWith("RULMENTUL")) {
				startStop = "BARTOLO_NORD_CAP";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\""
						+ route.getRoute_id()
						+ "/WEE_"
						+ route.getRoute_id()
						+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------

			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
			// L10
			// Triaj - Valea Cetatii
			// Valea Cetatii - Triaj
			if (startStop.startsWith("TRIAJ")
					&& endStop.startsWith("VALEA")) {
				startStop = "TRIAJ";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\""
						+ route.getRoute_id()
						+ "/WEE_"
						+ route.getRoute_id()
						+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}

			if (startStop.startsWith("VALEA")
					&& endStop.startsWith("TRIAJ")) {
				startStop = "VALEA_CETATII_CAP";
				firstQueryUrl = "{stop_id:\""
						+ startStop
						+ "\",\"trip_id\":\""
						+ route.getRoute_id()
						+ "/WEE_"
						+ route.getRoute_id()
						+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			}
			// -----------------------------------------------------------------------
			// -----------------------------------------------------------------------
						// L11
						// Triaj - Rulmentul
						// Rulmentul - Triaj
						if (startStop.startsWith("TRIAJ")
								&& endStop.startsWith("RULMENTUL")) {
							startStop = "TRIAJ";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("RULMENTUL")
								&& endStop.startsWith("TRIAJ")) {
							startStop = "RULMENTUL_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						// -----------------------------------------------------------------------
						
						// L12
						if (startStop.startsWith("STADIONUL TINERETULUI")
								&& endStop.startsWith("LIVADA")) {
							startStop = "STADION_TIN_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("STADIONUL TINERETULUI")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						// L14
						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("RASARIT")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("RASARIT")
								&& endStop.startsWith("LIVADA")) {
							startStop = "RASARITULUI_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
			
						// L16
						if (startStop.startsWith("STADIONUL MUNICIPAL")
								&& endStop.startsWith("LIVADA")) {
							startStop = "MUNICIPAL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L17
						if (startStop.startsWith("NOUA")
								&& endStop.startsWith("LIVADA")) {
							startStop = "NOUA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("NOUA")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L17b
						if (startStop.startsWith("TIMIS")
								&& endStop.startsWith("GARA")) {
							startStop = "BENZINARIA_PETROM";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("TIMIS")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L18
						if (startStop.startsWith("BARIERA")
								&& endStop.startsWith("STUPINI")) {
							startStop = "BARIERA_BARTOLOM";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("STUPINI")
								&& endStop.startsWith("BARIERA")) {
							startStop = "FUNDATURII_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L20
						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("POIANA")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("POIANA")
								&& endStop.startsWith("LIVADA")) {
							startStop = "POIANA_BRASOV";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L21
						if (startStop.startsWith("TRIAJ")
								&& endStop.startsWith("NOUA")) {
							startStop = "TRIAJ";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("NOUA")
								&& endStop.startsWith("TRIAJ")) {
							startStop = "NOUA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L22
						if (startStop.startsWith("SATURN")
								&& endStop.startsWith("STADIONUL TINERETULUI")) {
							startStop = "SATURN";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("STADIONUL TINERETULUI")
								&& endStop.startsWith("SATURN")) {
							startStop = "STADION_TIN_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L23
						if (startStop.startsWith("SATURN")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "SATURN";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("STADIONUL MUNICIPAL")
								&& endStop.startsWith("SATURN")) {
							startStop = "MUNICIPAL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L23b
						if (startStop.startsWith("TRIAJ")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "TRIAJ";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("STADIONUL MUNICIPAL")
								&& endStop.startsWith("TRIAJ")) {
							startStop = "MUNICIPAL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L24
						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("INSTITUTUL")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("INSTITUTUL")
								&& endStop.startsWith("LIVADA")) {
							startStop = "ICPC";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L24s
						if (startStop.startsWith("MUNICIPAL")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "MUNICIPAL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("TARG")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "TARG_AUTO";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L28b
						if (startStop.startsWith("BARTOLOMEU")
								&& endStop.startsWith("LIVADA")) {
							startStop = "BARTOLO_NORD_CAP";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("BARTOLOMEU")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L30
						if (startStop.startsWith("VALEA")
								&& endStop.startsWith("RULMENTUL")) {
							startStop = "VALEA_CETATII_CAP";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("RULMENTUL")
								&& endStop.startsWith("VALEA")) {
							startStop = "RULMENTUL_CAPAT";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L31
						if (startStop.startsWith("VALEA")
								&& endStop.startsWith("LIVADA")) {
							startStop = "VALEA_CETATII_CAP";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("VALEA")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L32
						if (startStop.startsWith("VALEA")
								&& endStop.startsWith("GARA")) {
							startStop = "VALEA_CETATII_CAP";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("VALEA")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L33
						if (startStop.startsWith("VALEA")
								&& endStop.startsWith("ROMAN")) {
							startStop = "VALEA_CETATII_CAP";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("ROMAN")
								&& endStop.startsWith("VALEA")) {
							startStop = "ROMAN";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L34
						if (startStop.startsWith("CET BRASOV")
								&& endStop.startsWith("LIVADA")) {
							startStop = "CET";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("CET BRASOV")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L34b
						if (startStop.startsWith("CET BRASOV (TIMIS-TRIAJ)")
								&& endStop.startsWith("LIVADA")) {
							startStop = "CET";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("CET BRASOV (TIMIS-TRIAJ)")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L35
						if (startStop.startsWith("NOUA")
								&& endStop.startsWith("GARA")) {
							startStop = "NOUA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("NOUA")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						
						// L36
						if (startStop.startsWith("INDEPENDENTEI")
								&& endStop.startsWith("LIVADA")) {
							startStop = "INDEPENDENTEI";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("INDEPENDENTEI")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L37
						if (startStop.startsWith("CRAITER")
								&& endStop.startsWith("LIVADA")) {
							startStop = "CRAITER";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("CRAITER")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L40
						if (startStop.startsWith("GARA")
								&& endStop.startsWith("STUPINI")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("STUPINI")
								&& endStop.startsWith("GARA")) {
							startStop = "STUPINII_IZVORUL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L41
						if (startStop.startsWith("STUPINI")
								&& endStop.startsWith("LIVADA")) {
							startStop = "STUPINI_CENTRU_IZVO";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("STUPINI")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L42
						if (startStop.startsWith("TIMIS-TRIAJ")
								&& endStop.startsWith("GARA")) {
							startStop = "TIMIS_TRIAJ";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("TIMIS-TRIAJ")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L44
						if (startStop.startsWith("STADIONUL MUNICIPAL")
								&& endStop.startsWith("MAGURELE")) {
							startStop = "MUNICIPAL";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("MAGURELE")
								&& endStop.startsWith("STADIONUL MUNICIPAL")) {
							startStop = "DEPOZITE_ILF";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L45
						if (startStop.startsWith("TRIAJ")
								&& endStop.startsWith("GARA")) {
							startStop = "TRIAJ";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("TRIAJ")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L50
						if (startStop.startsWith("TOCILE")
								&& endStop.startsWith("LIVADA")) {
							startStop = "TOCILE";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("TOCILE")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L50b
						if (startStop.startsWith("SOLOMON")
								&& endStop.startsWith("LIVADA")) {
							startStop = "SOLOMON";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("LIVADA")
								&& endStop.startsWith("SOLOMON")) {
							startStop = "LIVADA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L51
						if (startStop.startsWith("TOCILE")
								&& endStop.startsWith("GARA")) {
							startStop = "TOCILE";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("GARA")
								&& endStop.startsWith("TOCILE")) {
							startStop = "GARA";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
						// L52
						if (startStop.startsWith("TOCILE")
								&& endStop.startsWith("SATURN")) {
							startStop = "TOCILE";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/0/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}

						if (startStop.startsWith("SATURN")
								&& endStop.startsWith("TOCILE")) {
							startStop = "SATURN";
							firstQueryUrl = "{stop_id:\""
									+ startStop
									+ "\",\"trip_id\":\""
									+ route.getRoute_id()
									+ "/WEE_"
									+ route.getRoute_id()
									+ "/1/0\"}&fo=true&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
						}
						
			//Log.i(TAG, firstQueryUrl);
			StopTimes[] stopTimes = restTemplate.getForObject(baseUrl
					+ "{query}", StopTimes[].class, firstQueryUrl);

			// Got trip_id 1/WEE_1/0/0
			String secondQueryUrl;
			secondQueryUrl = "{\"trip_id\":\"" + stopTimes[0].getTrip_id()
					+ "\"}&f={\"_id\":0,\"pickup_type\":0,\"drop_off_type\":0}";
			StopTimes[] stopTimeses = restTemplate.getForObject(baseUrl
					+ "{query}", StopTimes[].class, secondQueryUrl);

			// https://api.mongolab.com/api/1/databases/rotransitnews/collections/stops?apiKey=4fccc029e4b067a61b69c449&q={%22stop_id%22:%22PRIMARIE%22}
			List<Stop> stops = new ArrayList<Stop>();
			for (StopTimes st : stopTimeses) {
				String stopQueryUrl = "{\"stop_id\":\"" + st.getStop_id()
						+ "\"}";
				//Log.i(TAG, stopQueryUrl);
				Stop[] stop = restTemplate
						.getForObject(
								"https://api.mongolab.com/api/1/databases/rotransitnews/collections/stops?apiKey=4fccc029e4b067a61b69c449&q="
										+ "{query}", Stop[].class, stopQueryUrl);
				stops.add(stop[0]);
			}

			return stops;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		protected void onPostExecute(List<Stop> result) {
			dismissProgressDialog();
			routeStops.put(route, result);
			routeName.setText("Route Name: " + route.getRoute_long_name());
			numberOfStops.setText("Number of stops: "
					+ routeStops.get(route).size());

			TextView stopsinRouteTv = new TextView(getApplicationContext());
			stopsinRouteTv.setText("Stops in Route: ");
			stopsinRouteTv.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			linearLayout.addView(stopsinRouteTv);

			for (Stop stop : routeStops.get(route)) {
				TextView textView = new TextView(getApplicationContext());
				textView.setText("Stop Name: " + stop.getStop_name());
				textView.setLayoutParams(new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT));
				linearLayout.addView(textView);
			}

			Button button = new Button(getApplicationContext());
			button.setText("Show Route On Map");
			button.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			linearLayout.addView(button);

			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Intent intent = new Intent(getApplicationContext(),
							MapsActivity_.class);

					intent.putParcelableArrayListExtra("stopsPerRoute",
							(ArrayList<? extends Parcelable>) routeStops
									.get(route));
					startActivity(intent);
				}
			});
		}
	}

	private void showLoadingProgressDialog() {
		this.showProgressDialog("Calculating Route" + "\n" + "Please wait...");
	}

	private void showProgressDialog(CharSequence message) {
		if (this.progressDialog == null) {
			this.progressDialog = new ProgressDialog(RouteDetailsActivity.this);
			this.progressDialog.setIndeterminate(true);
		}

		this.progressDialog.setMessage(message);
		this.progressDialog.show();
		this.progressDialog.setContentView(R.layout.custom_progress_bar);
	}

	private void dismissProgressDialog() {
		if (this.progressDialog != null && !destroyed) {
			this.progressDialog.dismiss();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		destroyed = true;
	}
}
