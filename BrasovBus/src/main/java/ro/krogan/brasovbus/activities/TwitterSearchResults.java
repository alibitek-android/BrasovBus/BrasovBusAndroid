package ro.krogan.brasovbus.activities;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import ro.krogan.brasovbus.models.Tweet;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterSearchResults {

	private List<Tweet> results;

	public void setResults(List<Tweet> results) {
		this.results = results;
	}

	public List<Tweet> getResults() {
		return results;
	}
}
