package ro.krogan.brasovbus.activities;

import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.androidannotations.annotations.UiThread;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ro.krogan.brasovbus.models.Route;
import ro.krogan.brasovbus.models.Stop;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EBean
public class MyBackgroundTask {

	@RootContext
	StopsActivity stopsActivity;

	@RootContext
	RoutesActivity routesActivity;

	@Background
	void stopActivityBackground() {
		String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/stops/?apiKey=4fccc029e4b067a61b69c449";
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getMessageConverters().add(
				new MappingJacksonHttpMessageConverter());

		Stop[] stops = restTemplate.getForObject(url, Stop[].class);
		updateStopsActivity(new TreeSet<Stop>(Arrays.asList(stops)));
	}

	@UiThread
	void updateStopsActivity(Set<Stop> stops) {
		// Log.i("STOPS=>>", stops.get(3).getStop_name());
		stopsActivity.setStopList(stops);
	}

	@Background
	void routesActivityBackground() {
		String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/routes/?apiKey=4fccc029e4b067a61b69c449";

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getMessageConverters().add(
				new MappingJacksonHttpMessageConverter());

		Route[] routes = restTemplate.getForObject(url, Route[].class);
		updateRoutesActivity(Arrays.asList(routes));
	}

	@UiThread
	void updateRoutesActivity(List<Route> routes) {
		routesActivity.setRouteList(routes);
	}
}
