package ro.krogan.brasovbus.activities;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.maps.MyItemizedOverlay;
import ro.krogan.brasovbus.models.Stop;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.googlecode.androidannotations.annotations.EActivity;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class StopsRoutesMapActivity extends MapActivity implements Runnable {

	MapController mapController;

	MyItemizedOverlay itemizedoverlay;
	MyItemizedOverlay firstStopItemizedoverlay;
	MyItemizedOverlay lastStopItemizedoverlay;

	MyLocationOverlay myLocOverlay;

	MapView myMapView;

	List<Overlay> mapOverlays;

	Location lastKnownLocation;

	private List<Stop> stopList;

	private ProgressDialog pd;

	private Thread thread;

	private boolean bLoading = false;

	// Set the center point
	// GeoPoint coordinates are based in microdegrees (degrees * 1e6)
	// 45.65282 25.6102
	GeoPoint centerPoint = new GeoPoint((int) (45.65282 * 1E6),
			(int) (25.6102 * 1E6));

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.stopsroutesmap_layout);

		final Bundle extras = getIntent().getExtras();

		Stop stop = null;
		List<Stop> stopsPerRoute = null;

		if (extras != null) {
			if (extras.containsKey("stop")) {
				stop = (Stop) extras.get("stop");
			}

			if (extras.containsKey("stopsPerRoute")) {
				stopsPerRoute = extras.getParcelableArrayList("stopsPerRoute");
			}
		}

		if (stop != null) {
			initMap();
			final String locationProvider = LocationManager.NETWORK_PROVIDER;
			final LocationManager locationManager = (LocationManager) StopsRoutesMapActivity.this
					.getSystemService(Context.LOCATION_SERVICE);
			lastKnownLocation = locationManager
					.getLastKnownLocation(locationProvider);

			GeoPoint point = new GeoPoint((int) (stop.getStop_lat() * 1E6),
					(int) (stop.getStop_lon() * 1E6));
			OverlayItem overlayitem = new OverlayItem(point,
					stop.getStop_name(), stop.getStop_lat() + " "
							+ stop.getStop_lon());
			itemizedoverlay.addOverlay(overlayitem);
			mapOverlays.add(itemizedoverlay);
			centerMapToLocation(point);
		} else if (stopsPerRoute != null) {
			initMap();
			final String locationProvider = LocationManager.NETWORK_PROVIDER;
			final LocationManager locationManager = (LocationManager) StopsRoutesMapActivity.this
					.getSystemService(Context.LOCATION_SERVICE);
			lastKnownLocation = locationManager
					.getLastKnownLocation(locationProvider);

			for (Stop s : stopsPerRoute) {
				if (s.equals(stopsPerRoute.get(0))) {
					GeoPoint pointFirst = new GeoPoint(
							(int) (s.getStop_lat() * 1E6),
							(int) (s.getStop_lon() * 1E6));
					OverlayItem overlayItemFirst = new OverlayItem(pointFirst,
							s.getStop_name(), s.getStop_lat() + " "
									+ s.getStop_lon());
					firstStopItemizedoverlay.addOverlay(overlayItemFirst);
					continue;
				}

				if (s.equals(stopsPerRoute.get(stopsPerRoute.size() - 1))) {
					GeoPoint pointLast = new GeoPoint(
							(int) (s.getStop_lat() * 1E6),
							(int) (s.getStop_lon() * 1E6));
					OverlayItem overlayitemLast = new OverlayItem(pointLast,
							s.getStop_name(), s.getStop_lat() + " "
									+ s.getStop_lon());
					lastStopItemizedoverlay.addOverlay(overlayitemLast);
					continue;
				}

				GeoPoint point = new GeoPoint((int) (s.getStop_lat() * 1E6),
						(int) (s.getStop_lon() * 1E6));
				OverlayItem overlayitem = new OverlayItem(point,
						s.getStop_name(), s.getStop_lat() + " "
								+ s.getStop_lon());
				itemizedoverlay.addOverlay(overlayitem);
			}

			mapOverlays.add(itemizedoverlay);
			mapOverlays.add(lastStopItemizedoverlay);
			mapOverlays.add(firstStopItemizedoverlay);
		} else {
			startThread();
		}
	}

	private void startThread() {
		bLoading = true;
		pd = ProgressDialog.show(this, "One moment please...",
				"Retrieving data", true, false);

		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		initData();
		handler.sendEmptyMessage(0);
	}

	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			pd.dismiss();
			initMap();
			final String locationProvider = LocationManager.NETWORK_PROVIDER;
			final LocationManager locationManager = (LocationManager) StopsRoutesMapActivity.this
					.getSystemService(Context.LOCATION_SERVICE);
			lastKnownLocation = locationManager
					.getLastKnownLocation(locationProvider);

			loadMap();
			mapOverlays.add(itemizedoverlay);

			bLoading = false;
		}
	};

	void initData() {
		String url = "https://api.mongolab.com/api/1/databases/rotransitnews/collections/stops/?apiKey=4fccc029e4b067a61b69c449";
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(
				new MappingJacksonHttpMessageConverter());
		Stop[] stops = restTemplate.getForObject(url, Stop[].class);
		stopList = Arrays.asList(stops);
	}

	private void initMap() {
		myMapView = (MapView) findViewById(R.id.stopsroutes_mapview);

		myMapView.setSatellite(false);
		// myMapView.setStreetView(true);
		myMapView.setBuiltInZoomControls(true);
		myMapView.displayZoomControls(true);

		mapController = myMapView.getController();
		mapController.setCenter(centerPoint);

		LocationManager locationManager;
		final String context = Context.LOCATION_SERVICE;
		locationManager = (LocationManager) getSystemService(context);

		final Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		locationManager.getBestProvider(criteria, true);

		initMyLocation();

		mapOverlays = myMapView.getOverlays();
		final Drawable drawable = this.getResources().getDrawable(
				R.drawable.redpin);
		final Drawable firstStopIcon = this.getResources().getDrawable(
				R.drawable.purplepin);
		final Drawable lastStopIcon = this.getResources().getDrawable(
				R.drawable.greenpin);

		itemizedoverlay = new MyItemizedOverlay(drawable, myMapView);
		if (getIntent().getExtras() != null
				&& getIntent().getExtras().containsKey("stopsPerRoute")) {
			firstStopItemizedoverlay = new MyItemizedOverlay(firstStopIcon,
					myMapView);
			lastStopItemizedoverlay = new MyItemizedOverlay(lastStopIcon,
					myMapView);
			firstStopItemizedoverlay.setBalloonBottomOffset(17);
			lastStopItemizedoverlay.setBalloonBottomOffset(17);
		}

		itemizedoverlay.setBalloonBottomOffset(17);
	}

	private void initMyLocation() {
		myLocOverlay = new MyLocationOverlay(this, myMapView);
		myLocOverlay.enableMyLocation();
		myMapView.getOverlays().add(myLocOverlay);
	}

	void loadMap() {
		for (Stop stop : stopList) {
			GeoPoint point = new GeoPoint((int) (stop.getStop_lat() * 1E6),
					(int) (stop.getStop_lon() * 1E6));
			// Log.i(stop.getStop_name(), stop.getStop_lat() + " " +
			// stop.getStop_lon());
			OverlayItem overlayitem = new OverlayItem(point,
					stop.getStop_name(), stop.getStop_lat() + " "
							+ stop.getStop_lon());
			itemizedoverlay.addOverlay(overlayitem);
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	private void centerMapToLocation(GeoPoint location) {
		mapController.animateTo(location);
		mapController.setZoom(17);
		myMapView.invalidate();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
}
