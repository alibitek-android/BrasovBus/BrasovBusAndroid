package ro.krogan.brasovbus.activities;

import java.util.List;

import org.springframework.http.ContentCodingType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Tweet;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.googlecode.androidannotations.annotations.EFragment;

import de.akquinet.android.androlog.Log;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EFragment
public class TweetsFragment extends ListFragment {

	protected static final String TAG = TweetsFragment.class.getSimpleName();

	private ProgressDialog progressDialog;

	private boolean destroyed = false;

	private List<Tweet> tweets;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		new TwitterSearchTask().execute("Brasov");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		destroyed = true;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (this.tweets == null) {
			return;
		}

		Tweet tweet = tweets.get(position);
		if (tweet != null) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://twitter.com/" + tweet.getFromUser()
							+ "/status/" + tweet.getId_str())));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		return inflater.inflate(R.layout.tweets_fragment_layout, container,
				false);
	}

	private class TwitterSearchTask extends
			AsyncTask<String, Void, ResponseEntity<TwitterSearchResults>> {

		@Override
		protected void onPreExecute() {
			showLoadingProgressDialog();
		}

		@Override
		protected ResponseEntity<TwitterSearchResults> doInBackground(
				String... params) {
			try {
				final String url = "http://search.twitter.com/search.json?q={query}&include_entities=true&rpp=25";

				HttpHeaders requestHeaders = new HttpHeaders();
				requestHeaders.setAcceptEncoding(ContentCodingType.GZIP);

				RestTemplate restTemplate = new RestTemplate();
				restTemplate.getMessageConverters().add(
						new MappingJacksonHttpMessageConverter());

				String searchValue = params[0];

				return restTemplate.exchange(url, HttpMethod.GET,
						new HttpEntity<Object>(requestHeaders),
						TwitterSearchResults.class, searchValue);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(ResponseEntity<TwitterSearchResults> result) {
			dismissProgressDialog();
			refreshResults(result);
			tweets = result.getBody().getResults();
		}
	}

	private void showLoadingProgressDialog() {
		this.showProgressDialog("Searching Twitter...");
	}

	private void showProgressDialog(CharSequence message) {
		if (this.progressDialog == null) {
			this.progressDialog = new ProgressDialog(getActivity());
			this.progressDialog.setIndeterminate(true);
		}

		this.progressDialog.setMessage(message);
		this.progressDialog.show();
	}

	private void dismissProgressDialog() {
		if (this.progressDialog != null && !destroyed) {
			this.progressDialog.dismiss();
		}
	}

	private void refreshResults(ResponseEntity<TwitterSearchResults> response) {
		if (response == null) {
			return;
		}

		TwitterSearchResults results = response.getBody();
		setListAdapter(new TweetListAdapter(getActivity(), results.getResults()));
	}
}
