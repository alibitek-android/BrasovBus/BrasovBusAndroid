package ro.krogan.brasovbus.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TabHost;
import com.googlecode.androidannotations.annotations.EActivity;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.utils.TabManager;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class NewsTabActivity extends FragmentActivity {

	TabHost mTabHost;

	TabManager mTabManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_layout);

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);

		mTabManager.addTab(
				mTabHost.newTabSpec("ratnews").setIndicator("RAT News"),
				RatNewsActivity.NewsTitlesFragment.class, null);
		mTabManager.addTab(
				mTabHost.newTabSpec("tweets").setIndicator("Tweets"),
				TweetsFragment.class, null);

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}
}
