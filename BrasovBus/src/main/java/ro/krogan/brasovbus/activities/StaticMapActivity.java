package ro.krogan.brasovbus.activities;

import ro.krogan.brasovbus.R;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity(R.layout.static_map)
public class StaticMapActivity extends Activity {

	@ViewById(R.id.staticMapWebView)
	WebView webView;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
	}

	@AfterViews
	void after() {
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		// webView.loadUrl("file:///android_asset/images/staticmap.jpg");
		webView.loadUrl("http://www.ratbv.ro/images/harta_mare.jpg");
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}
		});
	}
}
