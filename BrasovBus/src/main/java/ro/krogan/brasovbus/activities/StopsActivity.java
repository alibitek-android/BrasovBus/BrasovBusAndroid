package ro.krogan.brasovbus.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ro.krogan.brasovbus.BrasovBusApp;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Favorite;
import ro.krogan.brasovbus.models.Stop;
import android.app.ListActivity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.NonConfigurationInstance;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class StopsActivity extends ListActivity {

	private static final String TAG = StopsActivity.class.getSimpleName();

	@NonConfigurationInstance
	@Bean
	MyBackgroundTask task;

	@App
	BrasovBusApp application;
	
	private Set<Stop> stopList;

	private final Object mLock = new Object();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.stops_layout);
		task.stopActivityBackground();
		registerForContextMenu(getListView());
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(stopList.toArray(new Stop[stopList.size()])[info.position]
				.getStop_name());

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.stops_context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		Stop stop = (Stop) getListAdapter().getItem(info.position);

		switch (item.getItemId()) {
		case R.id.viewStopOnMap:
		{
			Toast.makeText(this, "click: " + stop.getStop_name(),
					Toast.LENGTH_SHORT).show();
			Intent mapIntent = new Intent(this, MapsActivity_.class);
			mapIntent.putExtra("stop", (Parcelable) stop);
			startActivity(mapIntent);
			return true;
		}
		case R.id.addStopToFavorites:
		{
			application.favorites.add(new Favorite(stop));
			return true;
		}	
		default:
			return super.onContextItemSelected(item);
		}
	}

	@ItemClick
	void listItemClicked(Stop stop) {
		Toast.makeText(this, "click: " + stop.getStop_name(),
				Toast.LENGTH_SHORT).show();
		Intent i = new Intent(this, StopsDetailsActivity_.class);
		i.putExtra("stop", (Parcelable) stop);
		startActivity(i);
	}

	public void setStopList(Set<Stop> stops) {
		stopList = stops;
		setListAdapter(new StopsAdapter());
	}

	private class StopsAdapter extends ArrayAdapter<Stop> {

		private StopsFilter mFilter;

		private ArrayList<Stop> mOriginalValues;

		public StopsAdapter() {
			super(StopsActivity.this, R.layout.stop_row, R.id.label,
					new ArrayList<Stop>(stopList));
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.stop_row, parent, false);

			TextView label = (TextView) row.findViewById(R.id.label);

			List<Stop> stops = new ArrayList<Stop>(stopList);
			label.setText(stops.get(position).getStop_name());

			TextView details = (TextView) row.findViewById(R.id.details_line);
			String description = "Latitude "
					+ String.valueOf(stops.get(position).getStop_lat()) + " "
					+ "Longitude "
					+ String.valueOf(stops.get(position).getStop_lon());
			details.setText(description);

			return row;
		}

		@Override
		public Stop getItem(int position) {
			return stopList.toArray(new Stop[stopList.size()])[position];
		}

		@Override
		public int getCount() {
			return stopList.size();
		}

		@Override
		public Filter getFilter() {
			if (mFilter == null) {
				mFilter = new StopsFilter();
			}
			return mFilter;
		}

		private class StopsFilter extends Filter {

			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				if (mOriginalValues == null) {
					synchronized (mLock) {
						mOriginalValues = new ArrayList<Stop>(stopList);
					}
				}

				if (charSequence == null || charSequence.length() == 0) {
					ArrayList<Stop> list;
					synchronized (mLock) {
						list = new ArrayList<Stop>(mOriginalValues);
					}
					results.values = list;
					results.count = list.size();
				} else {
					String prefixString = charSequence.toString().toLowerCase();

					ArrayList<Stop> values;
					synchronized (mLock) {
						values = new ArrayList<Stop>(mOriginalValues);
					}

					final int count = values.size();
					final ArrayList<Stop> newValues = new ArrayList<Stop>();

					for (int i = 0; i < count; i++) {
						final Stop value = values.get(i);

						final String valueText = value.getStop_name()
								.toLowerCase();

						if (valueText.contains(prefixString.toLowerCase())) {
							newValues.add(value);
						} else {
							final String[] words = valueText.split(" ");
							final int wordCount = words.length;

							for (int k = 0; k < wordCount; k++) {
								if (words[k].toLowerCase().contains(
										prefixString.toLowerCase())) {
									newValues.add(value);
									break;
								}
							}
						}
					}

					results.values = newValues;
					results.count = newValues.size();
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				stopList = new TreeSet<Stop>((List<Stop>) filterResults.values);
				if (filterResults.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.stops_menu, menu);

		SearchView searchView = (SearchView) menu.findItem(R.id.searchStop)
				.getActionView();

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		ComponentName cn = new ComponentName(this, StopsActivity_.class);
		SearchableInfo info = searchManager.getSearchableInfo(cn);
		searchView.setSearchableInfo(info);

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@SuppressWarnings("unchecked")
			@Override
			public boolean onQueryTextSubmit(String s) {
				ArrayAdapter<Stop> adapter = (ArrayAdapter<Stop>) getListAdapter();
				adapter.getFilter().filter(s.toLowerCase());
				return true;
			}

			@SuppressWarnings("unchecked")
			@Override
			public boolean onQueryTextChange(String s) {
				ArrayAdapter<Stop> adapter = (ArrayAdapter<Stop>) getListAdapter();
				adapter.getFilter().filter(s.toLowerCase());
				return true;
			}
		});

		return true;
	}
}
