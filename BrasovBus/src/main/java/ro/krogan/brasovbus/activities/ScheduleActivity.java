package ro.krogan.brasovbus.activities;

import java.util.ArrayList;
import java.util.List;

import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.models.Route;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class ScheduleActivity extends Activity implements
		OnItemSelectedListener {

	Spinner spinner;
	WebView webView;
	
	@Extra
	Route route;

	boolean inversed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule_layout);

		spinner = (Spinner) findViewById(R.id.scheduleSpinner);
		webView = (WebView) findViewById(R.id.scheduleWebView);
		//webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDisplayZoomControls(false);
		//webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		inversed = getIntent().getBooleanExtra("inversed", false);
		if (route != null) {
			spinner.setVisibility(View.GONE);
			if (inversed) {
				webView.loadUrl("http://www.ratbv.ro/afisaje/"
						+ route.getRoute_desc().replace("L", "") + "-intors.html");
			}
			else {
				webView.loadUrl("http://www.ratbv.ro/afisaje/"
						+ route.getRoute_desc().replace("L", "") + "-dus.html");			
			}
			webView.setWebViewClient(new WebViewClient() {
				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
				}
			});
		} else {
			List<String> list = new ArrayList<String>();
			for (int i = 1; i <= 52; i++) {
				list.add("L" + i);
			}

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, list);

			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(dataAdapter);

			spinner.setOnItemSelectedListener(this);	
		}		
	}

	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		String i = parent.getItemAtPosition(pos).toString().replace("L", "");
		webView.loadUrl("http://www.ratbv.ro/afisaje/"
				+ i + "-dus.html");
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}
		});
	}

	public void onNothingSelected(AdapterView<?> parent) {
	}
}
