package ro.krogan.brasovbus.activities;

import java.util.Timer;
import java.util.TimerTask;

import ro.krogan.brasovbus.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.googlecode.androidannotations.annotations.EActivity;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity(R.layout.splash)
public class SplashActivity extends Activity {

	private static final String TAG = SplashActivity.class.getSimpleName();

	private final long splashDelay = 2000;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate()");

		final TimerTask task = new TimerTask() {

			@Override
			public void run() {
				finish();
				final Intent mainIntent = new Intent().setClass(
						getApplicationContext(), MainActivity_.class);
				startActivity(mainIntent);
			}
		};

		final Timer timer = new Timer();
		SharedPreferences getPrefs = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		boolean showSplash = getPrefs.getBoolean("isSplashEnabled", true);
		if (showSplash) {
			timer.schedule(task, splashDelay);
		} else {
			final Intent mainIntent = new Intent().setClass(
					getApplicationContext(), MainActivity_.class);
			startActivity(mainIntent);
		}
	}
}
