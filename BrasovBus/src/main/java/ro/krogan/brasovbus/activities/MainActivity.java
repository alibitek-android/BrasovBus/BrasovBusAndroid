package ro.krogan.brasovbus.activities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Locale;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import ro.krogan.brasovbus.BrasovBusApp;
import ro.krogan.brasovbus.R;
import ro.krogan.brasovbus.feedback.FeedbackActivity;
import ro.krogan.brasovbus.models.Favorite;
import ro.krogan.brasovbus.preferences.PreferenceFragmentActivity;
import ro.krogan.brasovbus.services.AgencyService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.NonConfigurationInstance;
import com.googlecode.androidannotations.annotations.ViewById;
import com.googlecode.androidannotations.annotations.rest.RestService;

/**
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();

	@App
	BrasovBusApp application;

	@ViewById(R.id.home_btn_schedule)
	Button scheduleButton;

	@ViewById(R.id.home_btn_map)
	Button mapButton;

	@ViewById(R.id.home_btn_routes)
	Button routesButton;

	@ViewById(R.id.home_btn_stops)
	Button stopsButton;

	@ViewById(R.id.home_btn_favorites)
	Button favoritesButton;

	@ViewById(R.id.home_btn_news)
	Button newsButton;

	@ViewById(R.id.home_btn_notes)
	Button notesButton;

	@ViewById(R.id.home_btn_fares)
	Button faresButton;

	@RestService
	AgencyService agencyService;

	@ViewById(R.id.agencyNameTextView)
	TextView agencyNameTextView;

	@NonConfigurationInstance
	@Bean
	MainBackgroundTask task;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate()");

		setContentView(R.layout.dashboard_layout);

		agencyService.getRestTemplate().getMessageConverters()
				.add(new MappingJacksonHttpMessageConverter());

		task.mainActivityBackground();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateLanguage(
				this,
				PreferenceManager.getDefaultSharedPreferences(this).getString(
						getString(R.string.language), "English"));
		/*
		 * finish(); Intent myIntent = new Intent(MainActivity.this,
		 * MainActivity_.class); startActivity(myIntent);
		 */
	}

	@Click(R.id.home_btn_schedule)
	void doSchedule() {
		final Intent i = new Intent(getApplicationContext(),
				ScheduleActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_map)
	void doMap() {
		final Intent i = new Intent(getApplicationContext(),
				MapsActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_routes)
	void doRoutes() {
		final Intent i = new Intent(getApplicationContext(),
				RoutesActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_stops)
	void doStops() {
		final Intent i = new Intent(getApplicationContext(),
				StopsActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_favorites)
	void doFavorites() {
		final Intent i = new Intent(getApplicationContext(),
				FavoritesActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_news)
	void doNews() {
		final Intent i = new Intent(getApplicationContext(),
				NewsTabActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_notes)
	void doNotes() {
		final Intent i = new Intent(getApplicationContext(),
				PhotosActivity_.class);
		startActivity(i);
	}

	@Click(R.id.home_btn_fares)
	void doFares() {
		final Intent i = new Intent(getApplicationContext(),
				FaresTabActivity_.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home: {
			// app icon in action bar clicked; go home
			final Intent intent = new Intent(this, MainActivity_.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		}
		case R.id.preferences: {
			final Intent intent = new Intent(this,
					PreferenceFragmentActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.about: {
			final Intent intent = new Intent(this, AboutActivity_.class);
			startActivity(intent);
			break;
		}
		case R.id.feedback: {
			final Intent intent = new Intent(this, FeedbackActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.help: {
			final Intent intent = new Intent(this, HelpActivity_.class);
			startActivity(intent);
			break;
		}
		case R.id.exit: {
			finish();
			break;
		}
		default:
			return super.onOptionsItemSelected(item);
		}
		return false;
	}

	public void showAgencyText(String text) {
		agencyNameTextView.setText(text);
	}

	public static void updateLanguage(Context context, String idioma) {
		if (!"".equals(idioma)) {
			if ("English".equals(idioma)) {
				idioma = "en_US";
			} else if ("Romanian".equals(idioma)) {
				idioma = "ro_RO";
			}
			Locale locale = new Locale(idioma);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			context.getResources().updateConfiguration(config, null);
		}
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		/*
		 * SharedPreferences keyValues =
		 * this.getSharedPreferences("name_icons_list", Context.MODE_PRIVATE);
		 * SharedPreferences.Editor keyValuesEditor = keyValues.edit();
		 * 
		 * for (Favorite f : application.favorites) {
		 * keyValuesEditor.pustString(s, nameIcons.get(s)); }
		 */

		FileOutputStream outStream;
		try {
			outStream = new FileOutputStream(BrasovBusApp.FAVLIST_FILENAME);
			ObjectOutputStream objectOutStream = new ObjectOutputStream(
					outStream);
			objectOutStream.writeInt(application.favorites.size());

			for (Favorite f : application.favorites)
				try {
					objectOutStream.writeObject(f);
				} catch (IOException e) {
					e.printStackTrace();
				}

			objectOutStream.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
