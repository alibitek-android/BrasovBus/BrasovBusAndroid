package ro.krogan.brasovbus.activities;

import ro.krogan.brasovbus.R;
import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.widget.TabHost;

import com.googlecode.androidannotations.annotations.EActivity;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@SuppressWarnings("deprecation")
@EActivity
public class MapsActivity extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_layout);

		ActionBar bar = getActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		
		TabHost tabHost = getTabHost();

		TabHost.TabSpec mapSpec = tabHost.newTabSpec("Map");
		mapSpec.setIndicator("Map",
				getResources().getDrawable(R.drawable.icon_maps_tab));
		Intent stopsRoutesIntent = new Intent(this,
				StopsRoutesMapActivity_.class);
		if (getIntent().getExtras() != null) {
			if (getIntent().getExtras().containsKey("stop")) {
				stopsRoutesIntent.putExtra("stop", (Parcelable) getIntent()
						.getExtras().get("stop"));
			}

			if (getIntent().getExtras().containsKey("stopsPerRoute")) {
				stopsRoutesIntent.putExtra("stopsPerRoute", getIntent()
						.getExtras().getParcelableArrayList("stopsPerRoute"));
			}
		}
		mapSpec.setContent(stopsRoutesIntent);

		TabHost.TabSpec staticMapSpec = tabHost.newTabSpec("StaticMap");
		staticMapSpec.setIndicator("Static Map",
				getResources().getDrawable(R.drawable.icon_staticmap_tab));
		Intent videosIntent = new Intent(this, StaticMapActivity_.class);
		staticMapSpec.setContent(videosIntent);

		TabHost.TabSpec scheduleSpec = tabHost.newTabSpec("Schedule");

		scheduleSpec.setIndicator("Schedule",
				getResources().getDrawable(R.drawable.icon_photos_tab));
		Intent photosIntent = new Intent(this, ScheduleActivity_.class);
		scheduleSpec.setContent(photosIntent);

		tabHost.addTab(mapSpec);
		tabHost.addTab(staticMapSpec);
		tabHost.addTab(scheduleSpec);

		tabHost.setCurrentTab(0);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{    
	   switch (item.getItemId()) 
	   {        
	      case android.R.id.home:            
	         Intent intent = new Intent(this, MainActivity_.class);            
	         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
	         startActivity(intent);            
	         return true;        
	      default:            
	         return super.onOptionsItemSelected(item);    
	   }
	}
}
