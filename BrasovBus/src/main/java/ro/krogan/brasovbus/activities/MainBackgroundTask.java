package ro.krogan.brasovbus.activities;

import ro.krogan.brasovbus.models.Agency;

import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.androidannotations.annotations.UiThread;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EBean
public class MainBackgroundTask {

	@RootContext
	MainActivity activity;

	@Background
	void mainActivityBackground() {
		Agency agency = activity.agencyService.getAgency();
		updateMainActivity(agency.getAgency_name());
	}

	@UiThread
	void updateMainActivity(String result) {
		activity.showAgencyText(result);
	}
}
