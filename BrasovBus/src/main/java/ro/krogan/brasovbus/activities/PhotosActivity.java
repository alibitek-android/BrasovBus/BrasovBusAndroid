package ro.krogan.brasovbus.activities;

import ro.krogan.brasovbus.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.googlecode.androidannotations.annotations.EActivity;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EActivity
public class PhotosActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.photos_layout);

		final ImageView imagevew = (ImageView) findViewById(R.id.imgview);
		Gallery gallryview = (Gallery) findViewById(R.id.galleryview);
		gallryview.setAdapter(new ImageAdapter(this));
		gallryview
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						imagevew.setImageBitmap(decodeSampledBitmapFromResource(
								getResources(), view.getId(), 100, 100));
					}
				});
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public class ImageAdapter extends BaseAdapter {

		int mGalleryItemBackground;
		private Context mContext;
		private Integer[] mImageIds = { R.drawable.a01, R.drawable.a02,
				R.drawable.a03, R.drawable.a04, R.drawable.a05, R.drawable.a06,
				R.drawable.a07, R.drawable.a08, R.drawable.a09, };

		public ImageAdapter(Context c) {
			this.mContext = c;
			TypedArray a = obtainStyledAttributes(R.styleable.HelloGallery);
			mGalleryItemBackground = a.getResourceId(
					R.styleable.HelloGallery_android_galleryItemBackground, 0);
			a.recycle();
		}

		@Override
		public int getCount() {
			return mImageIds.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView iv = new ImageView(mContext);
			iv.setImageResource(mImageIds[position]);
			iv.setId(mImageIds[position]);
			iv.setLayoutParams(new Gallery.LayoutParams(120, 160));
			iv.setScaleType(ImageView.ScaleType.FIT_XY);
			iv.setBackgroundResource(mGalleryItemBackground);
			return iv;
		}
	}
}
