package ro.krogan.brasovbus.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.google.android.maps.GeoPoint;

import java.util.List;

public class GeoUtils {

	/**
	 * To be used for a one-shot best last location. Iterates over all providers
	 * and returns the most accurate result.
	 */
	public static Location getBestLastGeolocation(Context context) {
		LocationManager manager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = manager.getAllProviders();

		Location bestLocation = null;
		for (String it : providers) {
			Location location = manager.getLastKnownLocation(it);
			if (location != null) {
				if (bestLocation == null
						|| location.getAccuracy() < bestLocation.getAccuracy()) {
					bestLocation = location;
				}
			}
		}

		return bestLocation;
	}

	public static GeoPoint locationToGeoPoint(Location location) {
		if (location != null) {
			return new GeoPoint((int) (location.getLatitude() * 1E6 + 0.5),
					(int) (location.getLongitude() * 1E6 + 0.5));
		} else {
			return null;
		}
	}

	public static GeoPoint stringLocationToGeoPoint(String strlat, String strlon) {
		try {
			double lat = Double.parseDouble(strlat);
			double lon = Double.parseDouble(strlon);
			return new GeoPoint((int) (lat * 1E6 + 0.5),
					(int) (lon * 1E6 + 0.5));
		} catch (Exception ex) {
			return null;
		}
	}
}
