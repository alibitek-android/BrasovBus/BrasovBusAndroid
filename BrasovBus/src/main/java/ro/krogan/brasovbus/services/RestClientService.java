package ro.krogan.brasovbus.services;

import com.googlecode.androidannotations.annotations.rest.Rest;
import org.springframework.web.client.RestTemplate;

/**
 * Complete Resource URL:
 * https://api.mongolab.com/api/1/databases/rotransitnews/
 * collections?apiKey=4fccc029e4b067a61b69c449 q={"agency_id":"/{agency_id}/"}&
 * 
 * @author Alex Butum <alexbutum@gmail.com>
 */
@Rest("https://api.mongolab.com/api/1/databases/rotransitnews/collections")
public interface RestClientService {

	RestTemplate getRestTemplate();

	void setRestTemplate(RestTemplate restTemplate);

	void setRootUrl(String rootUrl);
}
