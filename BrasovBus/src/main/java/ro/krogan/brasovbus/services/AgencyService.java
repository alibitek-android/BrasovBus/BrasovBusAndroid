package ro.krogan.brasovbus.services;

import org.springframework.web.client.RestTemplate;

import com.googlecode.androidannotations.annotations.rest.Accept;
import com.googlecode.androidannotations.annotations.rest.Get;
import com.googlecode.androidannotations.annotations.rest.Rest;
import com.googlecode.androidannotations.api.rest.MediaType;
import ro.krogan.brasovbus.models.Agency;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@Rest("https://api.mongolab.com/api/1/databases/rotransitnews/collections")
@Accept(MediaType.APPLICATION_JSON)
public interface AgencyService {

	@Get("/agency?apiKey=4fccc029e4b067a61b69c449&fo=true")
	Agency getAgency();

	RestTemplate getRestTemplate();

	void setRestTemplate(RestTemplate restTemplate);
}
