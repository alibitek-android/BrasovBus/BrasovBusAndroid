package ro.krogan.brasovbus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ro.krogan.brasovbus.models.Favorite;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.googlecode.androidannotations.annotations.EApplication;

import de.akquinet.android.androlog.Log;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
@EApplication
public class BrasovBusApp extends Application {

	private static final String TAG = BrasovBusApp.class.getSimpleName();
	public static final String FAVLIST_FILENAME = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ File.separator
			+ "/favlist.dat";
	public static final boolean DEBUG = false;

	public List<Favorite> favorites = new ArrayList<Favorite>();

	private String agencyName;

	private Locale locale = null;

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (locale != null) {
			newConfig.locale = locale;
			Locale.setDefault(locale);
			getBaseContext().getResources().updateConfiguration(newConfig,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		FileInputStream inStream;
		try {
			inStream = new FileInputStream(FAVLIST_FILENAME);
			ObjectInputStream objectInStream = new ObjectInputStream(inStream);
			int count = objectInStream.readInt();
			List<Favorite> rl = new ArrayList<Favorite>();

			for (int c = 0; c < count; c++)
				rl.add((Favorite) objectInStream.readObject());

			objectInStream.close();

			setFavorites(rl);
			// ((ArrayAdapter<Favorite>)
			// getListAdapter()).notifyDataSetChanged();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		Configuration config = getBaseContext().getResources()
				.getConfiguration();

		String lang = settings.getString(getString(R.string.language), "");
		if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
			Log.i(TAG, "LANG=> " + lang);
			locale = new Locale(lang);
			Locale.setDefault(locale);
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

	public void setFavorites(List<Favorite> rl) {
		if (favorites == null || favorites.size() == 0) {
			favorites = rl;
		} else {
			favorites.addAll(rl);
		}
	}
}
