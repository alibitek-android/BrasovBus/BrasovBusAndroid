package ro.krogan.brasovbus.feedback;

import ro.krogan.brasovbus.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

public class FeedbackActivity extends Activity {

	Button submitButton;
	EditText emailField;
	RatingBar ratingBar;
	EditText nameField;
	EditText commentsField;

	public static AmazonClientManager clientManager = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback_layout);

		nameField = (EditText) findViewById(R.id.nameField);
		emailField = (EditText) findViewById(R.id.emailField);
		ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		commentsField = (EditText) findViewById(R.id.commentsField);
		submitButton = (Button) findViewById(R.id.submitButton);

		clientManager = new AmazonClientManager();

		if (clientManager.hasCredentials()) {
			submitButton.setVisibility(View.VISIBLE);
			this.wireButtons();
		} else {
			this.displayCredentialsIssueAndExit();
		}
	}

	/** Configure onClick handlers for buttons */
	private void wireButtons() {
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendFeedback();
			}
		});
	}

	/** Check data that was entered and send email if valid */
	protected void sendFeedback() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);

		if (commentsField.getText().length() == 0
				|| nameField.getText().length() == 0) {
			confirm.setTitle("Feedback Not Sent!");
			confirm.setMessage("Please fill out the form before submitting.");
			confirm.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
		} else if (SESManager.sendFeedbackEmail(commentsField.getText()
				.toString(), nameField.getText().toString(), ratingBar
				.getRating(), emailField.getText().toString())) {
			confirm.setTitle("Feedback Success!");
			confirm.setMessage("Thank you for your feedback.");
			confirm.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
		} else {
			confirm.setTitle("Feedback Failed!");
			confirm.setMessage("Unable to send feedback at this time.");
			confirm.setNegativeButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							FeedbackActivity.this.finish();
						}
					});
		}

		confirm.show();

	}

	/** Display an error and exit if credentials aren't provided */
	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle("Credential Problem!");
		confirm.setMessage("AWS Credentials not configured correctly.");
		confirm.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				FeedbackActivity.this.finish();
			}
		});
		confirm.show().show();
	}
}
