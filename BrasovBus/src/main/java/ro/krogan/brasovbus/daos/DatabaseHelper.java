package ro.krogan.brasovbus.daos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Alex Butum <alexbutum@gmail.com>
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = DatabaseHelper.class.getSimpleName();
	private SQLiteDatabase _database;
	private final Context _context;

	private static final String DATABASE_NAME = "brasovbus.sqlite";
	private static final String DATABASE_PATH = "/data/data/ro.krogan.brasovbus/databases/";
	private static final String AGENCIES_TABLE = "agencies";
	private static final String STOPS_TABLE = "stops";
	private static final int DATABASE_VERSION = 1;

	public static final String KEY_ROWID = "id";
	public static final String KEY_AGENCYID = "agency_id";
	public static final String KEY_AGENCYNAME = "agency_name";
	public static final String KEY_AGENCYURL = "agency_url";
	public static final String KEY_AGENCYTIMEZONE = "agency_timezone";
	public static final String KEY_AGENCYLANG = "agency_lang";
	public static final String KEY_AGENCYPHONE = "agency_phone";

	public static final String KEY_STOPID = "id";
	public static final String KEY_SAGENCYID = "sagency_id";
	public static final String KEY_STOPCODE = "stop_code";
	public static final String KEY_STOPNAME = "stop_name";
	public static final String KEY_STOPDESC = "stop_desc";
	public static final String KEY_STOPLAT = "stop_lat";
	public static final String KEY_STOPLON = "stop_lon";
	public static final String KEY_ZONEID = "zone_id";
	public static final String KEY_STOPURL = "stop_url";
	public static final String KEY_LOCATIONTYPE = "location_type";
	public static final String KEY_PARENTSTATION = "parent_station";

	private static final String AGENCYDB_CREATE = "create table agencies (_id INTEGER primary key autoincrement, "
			+ "agency_id TEXT, "
			+ "agency_name TEXT not null, "
			+ "agency_url TEXT not null, "
			+ "agency_timezone TEXT not null, "
			+ "agency_lang TEXT, " + "agency_phone TEXT);";

	private static final String STOPSDB_CREATE = "create table stops (_id TEXT primary key, "
			+ "sagency_id INTEGER, "
			+ "stop_code TEXT, "
			+ "stop_name TEXT not null, "
			+ "stop_desc TEXT, "
			+ "stop_lat DOUBLE not null, "
			+ "stop_lon DOUBLE not null, "
			+ "zone_id INTEGER, "
			+ "stop_url TEXT, "
			+ "location_type BOOL, "
			+ "parent_station INTEGER);";

	public DatabaseHelper(final Context context) {
		super(context, DATABASE_NAME, null, 1);
		_context = context;
	}

	public DatabaseHelper(final Context context, final String name,
			final CursorFactory factory, final int version) {
		super(context, name, factory, version);
		_context = context;
	}

	@Override
	public void onCreate(final SQLiteDatabase database) {

	}

	@Override
	public void onUpgrade(final SQLiteDatabase database, final int oldVersion,
			final int newVersion) {

	}

	public void createDataBase() throws IOException {

		final boolean dbExist = checkDataBase();

		if (!dbExist) {
			getReadableDatabase();

			try {
				copyDataBase();
			} catch (final IOException e) {
				throw new Error("Error copying database", e);
			}
		}
	}

	private boolean checkDataBase() {
		SQLiteDatabase checkDB = null;

		try {
			final String myPath = DATABASE_PATH + DATABASE_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
		} catch (final SQLiteException e) {
			Log.i(TAG, "The database doesn't exist", e);
		}

		if (checkDB != null) {
			checkDB.close();
		}

		return checkDB != null ? true : false;
	}

	private void copyDataBase() throws IOException {
		final InputStream myInput = _context.getAssets().open(DATABASE_NAME);

		final String outFileName = DATABASE_PATH + DATABASE_NAME;

		final OutputStream myOutput = new FileOutputStream(outFileName);

		final byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void openDataBase() throws SQLException {
		_database = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME,
				null, SQLiteDatabase.OPEN_READONLY);
	}

	/**
	 * Used to create the initial tables.
	 */
	public void init() {
		_database.execSQL(AGENCYDB_CREATE);
		_database.execSQL(STOPSDB_CREATE);

		final ContentValues values = new ContentValues();
		values.put("_id", "0");
		values.put("agency_id", "RATBV");
		values.put("agency_name", "Regia Autonoma de Transport Brasov");
		values.put("agency_url", "http://www.ratbv.ro");
		values.put("agency_timezone", "Europe/Bucharest");
		values.put("agency_lang", "ro");
		values.put("agency_phone", "");
		_database.insert(AGENCIES_TABLE, null, values);
		values.clear();

		_database.setVersion(DATABASE_VERSION);
	}

	public void upgrade() {
		Log.w("" + this, "Upgrading database " + _database.getPath()
				+ " from version " + _database.getVersion() + " to "
				+ DATABASE_VERSION + ", which will destroy all old data");
	}

	@Override
	public void close() {
		if (_database != null) {
			_database.close();
		}
	}

	public long insertAgency(final String agency_id, final String agency_name,
			final String agency_url, final String agency_timezone,
			final String agency_lang, final String agency_phone) {
		final ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_AGENCYID, agency_id);
		initialValues.put(KEY_AGENCYNAME, agency_name);
		initialValues.put(KEY_AGENCYURL, agency_url);
		initialValues.put(KEY_AGENCYTIMEZONE, agency_timezone);
		initialValues.put(KEY_AGENCYLANG, agency_lang);
		initialValues.put(KEY_AGENCYPHONE, agency_phone);
		return _database.insert(AGENCIES_TABLE, null, initialValues);
	}

	public boolean deleteAgency(final long rowId) {
		final int agency = _database.delete(AGENCIES_TABLE, KEY_ROWID + "="
				+ rowId, null);
		final int stops = _database.delete(STOPS_TABLE, KEY_SAGENCYID + "="
				+ rowId, null);
		return ((agency + stops) > 0);
	}

	public boolean deleteAllAgencies() {
		final int agency = _database.delete(AGENCIES_TABLE, KEY_ROWID, null);
		final int stops = _database.delete(STOPS_TABLE, KEY_STOPID, null);
		return ((agency + stops) > 0);
	}

	public Cursor getAllAgencies() {
		return _database.query(AGENCIES_TABLE, new String[] { KEY_ROWID,
				KEY_AGENCYNAME }, null, null, null, null, null);
	}

	public Cursor getAgency(final long rowId) throws SQLException {
		final Cursor mCursor = _database.query(true, AGENCIES_TABLE,
				new String[] { KEY_ROWID, KEY_AGENCYID, KEY_AGENCYNAME,
						KEY_AGENCYURL, KEY_AGENCYTIMEZONE, KEY_AGENCYLANG,
						KEY_AGENCYPHONE }, KEY_ROWID + "=" + rowId, null, null,
				null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public boolean updateAgency(final long rowId, final String agency_id,
			final String agency_name, final String agency_url,
			final String agency_timezone, final String agency_lang,
			final String agency_phone) {
		final ContentValues args = new ContentValues();
		args.put(KEY_AGENCYID, agency_id);
		args.put(KEY_AGENCYNAME, agency_name);
		args.put(KEY_AGENCYURL, agency_url);
		args.put(KEY_AGENCYTIMEZONE, agency_timezone);
		args.put(KEY_AGENCYLANG, agency_lang);
		args.put(KEY_AGENCYPHONE, agency_phone);
		return _database.update(AGENCIES_TABLE, args, KEY_ROWID + "=" + rowId,
				null) > 0;
	}

	public Cursor getAllStops(final long rowId) throws SQLException {
		return _database.query(STOPS_TABLE, new String[] { KEY_STOPID,
				KEY_SAGENCYID, KEY_STOPCODE, KEY_STOPNAME, KEY_STOPDESC },
				KEY_SAGENCYID + "=" + rowId, null, null, null, null, null);
	}
}
