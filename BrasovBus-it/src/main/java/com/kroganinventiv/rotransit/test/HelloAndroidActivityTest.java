package com.kroganinventiv.rotransit.test;


import android.test.ActivityInstrumentationTestCase2;
import ro.krogan.brasovbus.activities.MainActivity;


public class HelloAndroidActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public HelloAndroidActivityTest() {
        super(MainActivity.class); 
    }

    public void testActivity() {
        MainActivity activity = getActivity();
        assertNotNull(activity);
    }
}

